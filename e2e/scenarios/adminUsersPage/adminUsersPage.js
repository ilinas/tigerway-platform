const BasePageObject = require('../../BasePageObject');

class AdminUsersPage extends BasePageObject {
	constructor(driver) {
		super(driver, { url: '/admin/users' });
	}

	pageContent() {
		return this.$('.users-page');
	}

	findTableRows() {
		return this.$$('.users-page tr');
	}
	findUserNameInput() {
		return  this.$('#email');
	}
	findCompanyInput() {
		return this.$('#companyName');
	}
	getSubmitFormBtn() {
		return this.$('.ant-btn.ant-btn-primary');
	}
}

module.exports = AdminUsersPage;
