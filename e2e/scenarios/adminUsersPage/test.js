const { describe, it, before, assert, expect } = require('../../utils');
const AdminUsersPage = require('./AdminUsersPage');

module.exports = (driver) => {
	describe('[Admin Users Page]', () => {
		let adminUsersPage;

		let userName = 'admin@emial.com';
		let userCompanyName = 'userCompanyName';

		before(() => {
			adminUsersPage = new AdminUsersPage(driver);
			return adminUsersPage.navigate();
		});

		it('should correctly home page', async () => {
			await adminUsersPage.pageContent().isDisplayed();
			const trs = await adminUsersPage.findTableRows();
			assert.isOk(trs.length > 0, 'adminUsersPage cards correctly displayed');
		});

		it('should correctly write user name to form', async () => {
			adminUsersPage.findUserNameInput().sendKeys(userName);

			expect(await adminUsersPage.findUserNameInput().getAttribute('value')).to.be.a('string');
			expect(await adminUsersPage.findUserNameInput().getAttribute('value')).to.equal('admin@emial.com');
		});

		it('should correctly write user companyName to form', async () => {
			adminUsersPage.findCompanyInput().sendKeys(userCompanyName);

			expect(await adminUsersPage.findCompanyInput().getAttribute('value')).to.be.a('string');
			expect(await adminUsersPage.findCompanyInput().getAttribute('value')).to.equal('userCompanyName');
		});

		it('should correctly add user to table', async () => {
			adminUsersPage.getSubmitFormBtn().click();
			const trs = await adminUsersPage.findTableRows();
			assert.isOk(trs.length > 3, 'add new user is correctly');
			// driver.sleep(1000);
			driver.close();
		});
	});
};

