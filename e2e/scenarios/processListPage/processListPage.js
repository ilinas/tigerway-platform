const BasePageObject = require('../../BasePageObject');

class processListPage extends BasePageObject {
	constructor(driver) {
		super(driver, { url: '/procs' });
	}

	pageContent() {
		return this.$('.processes-page');
	}

	findAddProcessButton() {
		return this.$$('.add-button__block');
	}
}

module.exports = processListPage;
