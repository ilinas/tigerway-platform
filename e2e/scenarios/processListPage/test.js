const { describe, it, before, assert } = require('../../utils');
const ProcessListPage = require('./processListPage');

module.exports = driver => {
	describe('[Process List Page]', () => {
		let processListPage;

		before(() => {
			processListPage = new ProcessListPage(driver);
			return processListPage.navigate();
		});

		it('should correctly processes list page', async () => {
			await processListPage.pageContent().isDisplayed();
			const addButton = await processListPage.findAddProcessButton();
			assert.isOk(addButton.length > 0, `add new process button(${addButton.length}) correctly displayed`);
		});
	});
};
