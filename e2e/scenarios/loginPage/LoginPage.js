const BasePageObject = require('../../BasePageObject');

class LoginPage extends BasePageObject {
	constructor(driver) {
		super(driver, { url: '/login' });
	}

	pageContent() {
		return this.$('.login-page');
	}

	findForm() {
		return this.$$('.login-page__login-form');
	}
}

module.exports = LoginPage;
