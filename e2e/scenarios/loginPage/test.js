const { describe, it, before, assert } = require('../../utils');
const LoginPage = require('./LoginPage');

module.exports = driver => {
	describe('[Login Page]', () => {
		let loginPage;

		before(() => {
			loginPage = new LoginPage(driver);
			return loginPage.navigate();
		});

		it('should correctly home page', async () => {
			await loginPage.pageContent().isDisplayed();
			const form = await loginPage.findForm();
			assert.isOk(form.length > 0, `login page forms(${form.length}) correctly displayed`);
			// driver.sleep(1000);
		});
	});
};
