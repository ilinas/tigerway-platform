const BasePageObject = require('../../BasePageObject');

class HomePage extends BasePageObject {
	constructor(driver) {
		super(driver, { url: '/' });
	}

	pageContent() {
		return this.$('.home-page');
	}

	findCards() {
		return this.$$('.ant-card');
	}
}

module.exports = HomePage;
