const { describe, it, before, assert } = require('../../utils');
const HomePage = require('./HomePage');

module.exports = driver => {
	describe('[Home Page]', () => {
		let homePage;

		before(() => {
			homePage = new HomePage(driver);
			return homePage.navigate();
		});

		it('should correctly home page', async () => {
			await homePage.pageContent().isDisplayed();
			const cards = await homePage.findCards();
			assert.isOk(cards.length > 0, `home page cards(${cards.length}) correctly displayed`);
			// driver.sleep(1000);
		});
	});
};
