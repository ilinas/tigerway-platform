const webdriver = require('selenium-webdriver');
const { describe, before, after, beforeEach, afterEach, config, fixClickBehavior } = require('../utils');

require('chromedriver');

describe('E2E', function () { // eslint-disable-line
	const driver = new webdriver.Builder().forBrowser('chrome').build();

	this.timeout(config.TIMEOUT);
	fixClickBehavior(driver);

	before(async () => {
		// Login
	});

	describe('Scenarios', () => {
		require('../scenarios/loginPage/test')(driver);
		require('../scenarios/homePage/test')(driver);
		require('../scenarios/processListPage/test')(driver);
		require('../scenarios/adminUsersPage/test')(driver);
	});

	after(() => {
		driver.quit();
	});

	// DELAY
	const DELAY = 1000;
	beforeEach(() => {
		return driver.sleep(DELAY);
	});
	afterEach(() => {
		return driver.sleep(DELAY);
	});
	//
});

