export const INPUTTYPES = {
	text: {
		code: 'text',
		name: 'Текстовый инпут',
		multi: false,
		id: 1
	},
	numeric: {
		code: 'numeric',
		name: 'Числовой инпут',
		multi: false,
		id: 2
	},
	checkbox: {
		code: 'checkbox',
		name: 'Чекбокс',
		multi: false,
		id: 3
	},
	select: {
		code: 'select',
		name: 'Селект',
		multi: true,
		id: 4
	},
	multiselect: {
		code: 'multiselect',
		name: 'Мультиселект',
		multi: true,
		id: 5
	},
	date: {
		code: 'date',
		name: 'Дата',
		multi: false,
		id: 6
	},
	textarea: {
		code: 'textarea',
		name: 'Текстовая область',
		multi: false,
		id: 9
	},
	radio: {
		code: 'radio',
		name: 'Радио',
		multi: true,
		id: 10
	}
};
