import { types, getSnapshot } from 'mobx-state-tree';
import { toJS } from 'mobx';
const { optional, maybe, array, boolean, string, model } = types;

import { processApi } from './../../api/processApi';

import { DiagramModel } from './../../components/pages/ProcessPages/EditProcessPage/DiagramComponents/DiagramModel';

import { ProcMiniModel } from './models/graphModels/procMiniModel';
import { NodeModel } from './models/graphModels/nodeModel';
import { ProcModel } from './models/graphModels/procModel';
import { NodeViewModel } from './models/graphModels/nodeViewModels/nodeViewModel';

export const ProcessStore = model('ProcessStore', {
	model: maybe(ProcModel),

	selectedNode: optional(NodeModel, {}),

	nodeViews: optional(array(NodeViewModel), []),

	processes: optional(array(ProcMiniModel), []),

	copyValue: optional(string, ''),
	procsSpin: optional(boolean, false)
})
	.views(self => ({
		get AllNodeViews() {
			return self.nodeViews;
		},
		get getSelectedNode() {
			// console.log('self.selectedNode', self.selectedNode);
			// console.log('getSelectedNode', self.model.nodes.find(node => node.selected === true));
			return self.model.nodes.find(node => node.selected === true);
		}
	}))
	.actions(self => {
		async function getAllProcesses() {
			let res = await processApi.getAllProcesses();
			self.processes = res.data.procs;
		}
		function onNodeSelected(selectedNode) {
			// self.model.onNodeSelected(selectedNode);
			// let snapshotSelectedNode = getSnapshot(self.model.nodes.find(node => node.id === selectedNode.id));
			// self.selectedNode = NodeModel.create(snapshotSelectedNode);
		}
		async function getAllData() {
			let res;
			try {
				res = await processApi.getAllData();
				self.nodeViews.replace(res.data.nodeView);
			}
			finally {
				// NProgress.done();
			}
		}

		async function getProcess(id) {
			try {
				let res = {};
				setSpin(true);
				res = await processApi.getProcess(id);
				if (res.success) {
					self.model.updateModel(res.data.proc);
					return toJS(self.model);
				}
			}
			finally {
				// console.log(toJS(self.model));
				setSpin(false);
			}
		}

		async function saveProc() {
			try {
				setSpin(true);
				let res = {};
				res = await processApi.createProcess(toJS(self.model));
				// console.log('save', data);
				if (res.success) {
					await getProcess(self.model.id);
				}
				return res.success;
			}
			finally {
				setSpin(false);
			}
		}

		async function updateProc() {
			try {
				setSpin(true);
				// console.log('update', toJS(self.model));
				let res = await processApi.updateProcess(toJS(self.model));
				if (res.success) {
					await getProcess(self.model.id);
				}
			}
			finally {
				// await getAllProcesses();
				setSpin(false);
			}
		}

		async function deleteProcess(id) {
			try {
				setSpin(true);
				await processApi.deleteProcess(id);
				await getAllProcesses();
			}
			finally {
				setSpin(false);
			}
		}

		function generateNewModel() {
			let newModel = new DiagramModel();
			self.model = newModel.serializeDiagram();
		}
		function setSpin(value) {
			self.procsSpin = value;
		}
		return {
			generateNewModel,

			onNodeSelected,

			getAllProcesses,
			getAllData,
			getProcess,
			saveProc,
			updateProc,
			deleteProcess
		};
	});
