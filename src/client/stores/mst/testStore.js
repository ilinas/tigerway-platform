import { types } from 'mobx-state-tree';
const { optional, maybe, array, boolean, string, number, reference, identifier, model } = types;
import { ParentModel } from './models/testModels/parentModel';
import { ChildModel } from './models/testModels/childModel';

export const TestStore = model('TestStore', {
	childs: optional(array(ChildModel), []),
	parents: optional(array(ParentModel), [])
}).actions(self => ({
	afterCreate() {
		self.childs = [{ id: 2, name: 'child2', parent: 10 }, { id: 1, name: 'child1', parent: 10 }];
		self.parents = [{ id: 3, name: 'parent3', child: 2 }, { id: 4, name: 'parent4', child: 1 }, { id: 10, name: 'parent10', child: 1 }];
	}
}));
