import React from 'react';
import { types } from 'mobx-state-tree';
const { model } = types;
import { notification, Icon } from 'antd';

export const NotifyStore = model('NotifyStore', {}).actions(self => ({
	showError(title = 'Ошибка', errors = 'Что-то пошло не так. Попробуйте позже.') {
		notification.open({
			message: title,
			description: errors,
			icon: <Icon type="close-circle-o" style={{ color: '#BB0000' }} />
		});
	},
	showSucces(title, message) {
		notification.open({
			message: title,
			description: message,
			icon: <Icon type="check-circle-o" style={{ color: '#00BB00' }} />
		});
	}
}));
