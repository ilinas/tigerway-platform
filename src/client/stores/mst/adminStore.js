import React from 'react';
import generator from 'generate-password';
import { types, getSnapshot } from 'mobx-state-tree';
const { optional, array, boolean, model, string } = types;
import { UserModel } from './models/adminModels/userModel';
import { adminApi } from './../../api/adminApi';

export const AdminStore = model('AdminStore', {
	users: optional(array(UserModel), []),
	editUser: optional(UserModel, {}),

	isLoading: optional(boolean, true),
	searchText: optional(string, ''),
	searchField: optional(string, '')
})
	.views(self => ({
		findEditUserById(id) {
			let userSnapshot = getSnapshot(self.users.find(user => user.id === id));
			self.editUser = UserModel.create(userSnapshot);
		},
		get searchData() {
			const { searchField, searchText, users } = self;
			if (searchField === '') {
				return users;
			}
			let match;
			let searchReg = new RegExp(searchText, 'gi');
			return users
				.map(record => {
					match = `${record[searchField]}`.match(searchReg);
					if (!match) {
						return null;
					}
					return {
						...record,
						[searchField]: (
							<span key={record.id}>
								{`${record[searchField]}`.split(searchReg).map(
									(text, i) =>
										i > 0
											? [
												<span key={i} className="highlight">
													{match[0]}
												</span>,
												text
											]
											: text
								)}
							</span>
						)
					};
				})
				.filter(record => !!record);
		}
	}))
	.actions(self => {
		async function getListUsers() {
			let res = {};
			try {
				setSpin(true);
				res = await adminApi.getAllAccounts();
				self.users = [];
				self.users = res;
			}
			finally {
				setSpin(false);
			}
		}
		async function addNewUser(userData) {
			let password = generator.generate({
				length: 14,
				numbers: true
			});
			try {
				setSpin(true);
				await adminApi.createAccount({ ...userData, plainPassword: password });
			}
			finally {
				setSpin(false);
				getListUsers();
			}
		}
		async function deleteUser(id) {
			try {
				setSpin(true);
				setTimeout(await adminApi.deleteAccount(id), 1000);
			}
			finally {
				setSpin(false);
				getListUsers();
			}
		}
		async function updateUser() {
			try {
				setSpin(true);
				await adminApi.upadateAccount(self.editUser);
			}
			finally {
				setSpin(false);
				getListUsers();
				self.editUser = {
					companyName: '',
					plainPassword: '',
					email: ''
				};
			}
		}

		function setSpin(value) {
			self.isLoading = value;
		}
		return {
			getListUsers,
			addNewUser,
			deleteUser,
			updateUser,

			setSpin
		};
	});
