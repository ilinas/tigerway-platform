import { types } from 'mobx-state-tree';
const { optional, string, number, boolean, model, maybe } = types;

import { integrationSmsApi } from './../../api/integrationSmsApi';

export const IntegrationSmsData = model('IntegrationSmsData', {
	id: maybe(number),
	username: optional(string, ''),
	apikey: optional(string, ''),
	service: optional(string, '')
});

export const IntegrationSmsStore = model('IntegrationSmsStore', {
	ingData: maybe(IntegrationSmsData),
	isLoading: optional(boolean, false)
}).actions(self => {
	async function getSmsData() {
		try {
			setLoading(true);

			let res = await integrationSmsApi.getIntegrationSmsData();
			if (!res.data.hasOwnProperty('length') && res.success) {
				self.ingData = res.data.smsIng;
			}
			else {
				self.ingData = {
					id: null,
					username: '',
					apikey: '',
					service: ''
				};
			}
		}
		finally {
			setLoading(false);
		}
	}
	async function setSmsData(data) {
		try {
			setLoading(true);
			if (self.ingData.id) {
				data.id = self.ingData.id;
			}
			let res = await integrationSmsApi.setIntegrationSmsData(data);
			if (res.success) {
				getSmsData();
			}
		}
		finally {
			setLoading(false);
		}
	}
	async function clearSmsData() {
		try {
			setLoading(true);
			let res = await integrationSmsApi.clearSmsData();
			if (res.success) {
				getSmsData();
			}
		}
		finally {
			setLoading(false);
		}
	}
	function setLoading(value) {
		self.isLoading = value;
	}
	return {
		getSmsData,
		setSmsData,
		clearSmsData,

		setLoading
	};
});
