import { ProcessStore } from './processStore';
import { ParamsStore } from './paramsStore';
import { TestStore } from './testStore';
import { NotifyStore } from './notifyStore';
import { AdminStore } from './adminStore';
import { IntegrationAmoStore } from './integrationAmoStore';
import { IntegrationSiteStore } from './integrationSiteStore';
import { IntegrationEmailStore } from './integrationEmailStore';
import { IntegrationSmsStore } from './integrationSmsStore';
import { EventsStore } from './eventsStore';
import { types, getSnapshot, onSnapshot, unprotect } from 'mobx-state-tree';
import { observable } from 'mobx';
import { DiagramModel } from './../../components/pages/ProcessPages/EditProcessPage/DiagramComponents/DiagramModel';

export const RootStore = types
	.model('RootStore', {
		processStore: types.optional(ProcessStore, {
			model: new DiagramModel().serializeDiagram()
		}),
		paramsStore: types.optional(ParamsStore, {}),
		testStore: types.optional(TestStore, {}),
		notifyStore: types.optional(NotifyStore, {}),
		adminStore: types.optional(AdminStore, {}),
		eventsStore: types.optional(EventsStore, {}),
		integrationSiteStore: types.optional(IntegrationSiteStore, {}),
		integrationEmailStore: types.optional(IntegrationEmailStore, {ingData:{
			login: '',
			subdomain: '',
			apikey: ''
		}}),
		integrationSmsStore: types.optional(IntegrationSmsStore, {ingData:{
			username: '',
			service: '',
			apikey: ''
		}}),
		integrationAmoStore: types.optional(IntegrationAmoStore, {
			formValues: {
				username: '',
				password: '',
				server: ''
			}
		})
	})
	.views(self => ({}))
	.actions(self => ({

	}));
const rootStore = RootStore.create();
unprotect(rootStore);
export default rootStore;
// undo / redo;
// export const recorder = observable({
// 	currentIndex: 0,
// 	snapshots: observable.shallowArray([getSnapshot(rootStore.processStore.model)]),
// 	replaying: false
// });
// onSnapshot(rootStore.processStore.model, snapshot => {
// 	if (!recorder.replaying) {
// 		recorder.snapshots.splice(recorder.currentIndex + 1);
// 		recorder.snapshots.push(snapshot);
// 		recorder.currentIndex++;
// 		// console.log('recorder', recorder.snapshots.slice());
// 	}
// });
