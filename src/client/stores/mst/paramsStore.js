import { types } from 'mobx-state-tree';
const { optional, array, model } = types;

import { masterdataApi } from './../../api/masterdataApi';
import { paramTypesApi } from './../../api/paramTypesApi';

import { ParamType } from './models/paramsModels/paramType';
import { ParamValueType } from './models/paramsModels/paramValueType';

export const ParamsStore = model('ParamsStore', {
	paramTypes: optional(array(ParamType), []),
	paramValueTypes: optional(array(ParamValueType), [])
}).actions(self => {
	async function getAllData() {
		let res = {};
		try {
			res = await masterdataApi.getAllData();
			// if (res) {
			// 	localStorage.setItem('paramsStoreData', JSON.stringify(res));
			// }
			// else {
			// let paramsStoreData = localStorage.getItem('paramsStoreData');
			// res = JSON.parse(paramsStoreData);
			// }
			self.paramValueTypes.replace(res.data.paramValueTypes);
			self.paramTypes.replace(res.data.paramTypes);
			// console.log('self.paramTypes', self.paramTypes.toJSON());
			// console.log('self.paramValueTypes', self.paramValueTypes.toJSON());
			// validationStore.validationTypes = res.data.validationTypes;
		}
		finally {
			// this.spin = false;
		}
	}
	async function createParam(paramData) {
		let fullData = this.mapParamStructure(paramData);
		let res = {};
		try {
			res = await paramTypesApi.createParam(fullData);
			if (res.success) {
				self.paramTypes.push(res.data.paramType);
				return res.success;
			}
		}
		finally {
			// this.spin = false;

		}
	}
	async function updateParam(paramData, id) {
		// let param = this.paramTypes.find(paramType => paramType.id === id * 1);
		// let fullData = Object.assign(toJS(param), paramData);
		let defaultData = { paramOptions: [], validation: {} };
		let fullData = Object.assign(defaultData, paramData);
		try {
			await paramTypesApi.updateParam({ id, ...fullData });
		}
		finally {
			// this.spin = false;

		}
	}
	async function deleteParam(id) {
		try {
			this.spin = true;
			let res = await paramTypesApi.deleteParam(id);
			if (res.success) {
				self.paramTypes = self.paramTypes.filter(paramType => paramType.id !== id);
			}
		}
		finally {
			// this.spin = false;
		}
	}

	function setValidationsArray(findArray, code) {
		let results = findArray.find(paramValueType => paramValueType.code === code);
		let validationTypesCodes = results.validationTypeCodes;
		// validationStore.getValidationTypes(validationTypesCodes);
	}

	function mapValidationParams(object, params) {
		// console.log(this.arrayToObject(toJS(params), 'code'));
		return this.arrayToObject(params, 'code');
	}

	function arrayToObject(arr, keyField) {
		Object.assign({}, ...arr.map(item => ({ [item[keyField]]: item })));
	}
	return {
		getAllData,
		createParam,
		deleteParam,
		updateParam,

		setValidationsArray,
		mapValidationParams
	};
});
