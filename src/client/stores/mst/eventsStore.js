import { types } from 'mobx-state-tree';
const { optional, string, frozen, array, boolean, model } = types;

// import {procEventModel} from ''

import { eventsApi } from './../../api/eventsApi';

export const EventsStore = model('EventsStore', {
	procEvents: optional(array(frozen), []),
	isLoading: optional(boolean, false)
}).actions(self => {
	async function getEventsData() {
		try {
			setLoading(true);
			let res = await eventsApi.getEventsData();
			if (res.success) {
				self.procEvents = res.data.procEvents;
				// console.log(self.procEvents);
			}
		}
		finally {
			setLoading(false);
			// no used finally
		}
	}
	function setLoading(loading) {
		self.isLoading = loading;
	}
	return {
		getEventsData,
		setLoading
	};
});
