import { types } from 'mobx-state-tree';
const { optional, string, model } = types;

import { integrationSiteApi } from './../../api/integrationSiteApi';

export const IntegrationSiteStore = model('IntegrationSiteStore', {
	apiKey: optional(string, '')
}).actions(self => {
	async function getApiKeyData() {
		try {
			let res = await integrationSiteApi.getIntegrationSiteData();
			if (res.success) {
				self.apiKey =  res.data.apikey;
			}
		}
		finally {
			// no used finally
		}
	}
	return {
		getApiKeyData
	};
});
