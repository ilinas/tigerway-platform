import { types, getParent } from 'mobx-state-tree';
const { optional, maybe, boolean, model } = types;

import { integrationAmoApi } from './../../api/integrationAmoApi';

import { IntegrationAmoDataModel } from './models/integrationModels/integrationAmoDataModel';

export const IntegrationAmoStore = model('IntegrationAmoStore', {
	formValues: maybe(IntegrationAmoDataModel),

	isLoading: optional(boolean, true),
	isRefreshSchema: optional(boolean, false)
}).actions(self => {
	async function getAmoData() {
		try {
			setLoadingSpin(true);
			let res = await integrationAmoApi.getIntegrationAmoData();
			if (res.data.length === 0) {
				self.formValues = {};
			}
			else {
				self.formValues.id = res.data.amoAccount.id;
				self.formValues.login = res.data.amoAccount.login;
				self.formValues.subdomain = res.data.amoAccount.subdomain;
				self.formValues.apikey = res.data.amoAccount.apikey;
			}
		}
		finally {
			setLoadingSpin(false);
		}
	}

	async function saveAmoData(data) {
		try {
			setLoadingSpin(true);
			let res = await integrationAmoApi.saveIntegrationAmoData(data);
			if (res.success) {
				getAmoData();
				let rootStore = getParent(self);
				rootStore.paramsStore.getAllData();
				rootStore.processStore.getAllData();
			}
		}
		finally {
			setLoadingSpin(false);
		}
	}

	async function updateAmoData(data) {
		try {
			setLoadingSpin(true);
			let res = await integrationAmoApi.updateIntegrationAmoData({ ...data, id: self.formValues.id });
			if (res.success) {
				getAmoData();
				let rootStore = getParent(self);
				rootStore.paramsStore.getAllData();
				rootStore.processStore.getAllData();
			}
		}
		finally {
			setLoadingSpin(false);
		}
	}

	async function refreshAmoData() {
		try {
			setRefreshShemaSpin(true);
			let res = await integrationAmoApi.refreshAmoData(self.formValues.id);
			if (res.success) {
				let rootStore = getParent(self);
				rootStore.paramsStore.getAllData();
				rootStore.processStore.getAllData();
			}
		}
		finally {
			setRefreshShemaSpin(false);
		}
	}

	async function clearAmoData() {
		try {
			setLoadingSpin(true);
			let res = await integrationAmoApi.clearAmoData(self.formValues.id);
			if (res.success) {
				self.formValues = { id: 0, login: '', subdomain: '', apikey: '' };
				let rootStore = getParent(self);
				rootStore.paramsStore.getAllData();
				rootStore.processStore.getAllData();
			}
		}
		finally {
			setLoadingSpin(false);
		}
	}

	function setLoadingSpin(value) {
		self.isLoading = value;
	}
	function setRefreshShemaSpin(value) {
		self.isRefreshSchema = value;
	}
	return {
		getAmoData,
		saveAmoData,
		updateAmoData,
		refreshAmoData,
		clearAmoData
	};
});
