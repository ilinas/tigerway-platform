import { types } from 'mobx-state-tree';
const { optional, string, number, boolean, model, maybe } = types;

import { integrationEmailApi } from './../../api/integrationEmailApi';

export const IntegrationEmailData = model('IntegrationEmailData', {
	id: maybe(number),
	username: optional(string, ''),
	password: optional(string, ''),
	server: optional(string, '')
});

export const IntegrationEmailStore = model('IntegrationEmailStore', {
	ingData: maybe(IntegrationEmailData),
	isLoading: optional(boolean, false)
}).actions(self => {
	async function getEmailData() {
		try {
			setLoading(true);

			let res = await integrationEmailApi.getIntegrationEmailData();
			if (!res.data.hasOwnProperty('length') && res.success) {
				self.ingData = res.data.emailIng;
			}
			else {
				self.ingData = {
					id: null,
					username: '',
					password: '',
					server: ''
				};
			}
		}
		finally {
			setLoading(false);
		}
	}
	async function setEmailData(data) {
		try {
			setLoading(true);
			if (self.ingData.id) {
				data.id = self.ingData.id;
			}
			let res = await integrationEmailApi.setIntegrationEmailData(data);
			if (res.success) {
				getEmailData();
			}
		}
		finally {
			setLoading(false);
		}
	}
	async function clearEmailData() {
		try {
			setLoading(true);
			let res = await integrationEmailApi.clearEmail();
			if (res.success) {
				getEmailData();
			}
		}
		finally {
			setLoading(false);
		}
	}
	function setLoading(value) {
		self.isLoading = value;
	}
	return {
		getEmailData,
		setEmailData,
		clearEmailData,

		setLoading
	};
});
