import { types, getParent } from 'mobx-state-tree';
const { optional, maybe, late, array, boolean, string, number, reference, model } = types;
import { ChildByIdReference } from './childModel';

export const RefParentModel = model('RefParentModel', {
	id: maybe(types.identifier(number)),
	child: ChildByIdReference,
	name: string
});

// export const Parent = model('Parent', {
// 	id: maybe(types.identifier(number)),
// 	name: string
// });

// export const ParentByIdReference = maybe(
// 	reference(Parent, {
// 		// given an identifier, find the user
// 		get(identifier, parent) {
// 			let parentParent = getParent(getParent(parent));
// 			return parentParent.parents.find(u => u.id === identifier) || null;
// 		},
// 		// given a user, produce the identifier that should be stored
// 		set(value) {
// 			return value.id;
// 		}
// 	})
// );
