import { types, getParent } from 'mobx-state-tree';
const { optional, maybe, late, array, boolean, string, number, reference, model } = types;
import { RefParentModel } from './refParentModel';

export const RefChildModel = model('RefChildModel', {
	id: maybe(types.identifier(number)),
	name: string,
	parent: late(() => RefParentModel)
});
