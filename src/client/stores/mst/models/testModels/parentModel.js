import { types, getParent } from 'mobx-state-tree';
const { optional, maybe, late, array, boolean, string, number, reference, model } = types;
import { ChildByIdReference } from './childModel';

export const ParentModel = model('ParentModel', {
	id: maybe(types.identifier(number)),
	child: late(() => ChildByIdReference),
	name: string
});

export const ParentByIdReference = maybe(
	reference(ParentModel, {
		get(identifier, parent) {
			let parentParent = getParent(getParent(parent));
			return parentParent.parents.find(u => u.id === identifier) || null;
		},
		set(value) {
			return value.id;
		}
	})
);
