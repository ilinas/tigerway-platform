import { types, getParent } from 'mobx-state-tree';
const { optional, maybe, late, array, boolean, string, number, reference, model } = types;
import { ParentByIdReference } from './parentModel';

export const ChildModel = model('ChildModel', {
	id: maybe(types.identifier(number)),
	name: string,
	parent: late(() => ParentByIdReference)
});

export const ChildByIdReference = maybe(
	reference(ChildModel, {
		get(identifier, parent) {
			let parentParent = getParent(getParent(parent));
			return parentParent.childs.find(u => u.id === identifier) || null;
		},
		set(value) {
			return value.id;
		}
	})
);
