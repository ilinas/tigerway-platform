import { types } from 'mobx-state-tree';
const { maybe, string, number, optional, model } = types;

export const IntegrationAmoDataModel = model('IntegrationAmoDataModel', {
	id: maybe(number),
	login: optional(string, ''),
	subdomain: optional(string, ''),
	apikey: optional(string, '')
});
