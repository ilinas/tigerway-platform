import { types } from 'mobx-state-tree';
const { optional, maybe, array, boolean, string, frozen, model, identifier } = types;
import { PointModel } from './pointModel';

export const LinkModel = model('LinkModel', {
	extras: optional(frozen, {}),
	id: optional(identifier(string), ''),
	points: optional(array(PointModel), []),
	selected: optional(boolean, false),
	source: maybe(string),
	sourcePort: maybe(string),
	target: maybe(string),
	targetPort: maybe(string),
	type: optional(string, ''),
	_class: optional(string, '')
});
