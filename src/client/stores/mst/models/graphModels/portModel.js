import { types } from 'mobx-state-tree';
const { optional, array, boolean, string, model, identifier } = types;

export const PortModel = model('PortModel', {
	id: optional(identifier(string), ''),
	in: optional(boolean, false),
	label: optional(string, ''),
	links: optional(array(string), []),
	name: optional(string, ''),
	parentNode: optional(string, ''),
	selected: optional(boolean, false),
	type: 'default',
	_class: optional(string, '')
});
