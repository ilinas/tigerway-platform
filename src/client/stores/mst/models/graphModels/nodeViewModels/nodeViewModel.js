import { types } from 'mobx-state-tree';
const { optional, array, string, model } = types;
import { NodeGroupModel } from './nodeGroupModel';

export const NodeViewModel = model('NodeViewModel', {
	code: string,
	name: string,
	nodeGroups: optional(array(NodeGroupModel), [])
});
