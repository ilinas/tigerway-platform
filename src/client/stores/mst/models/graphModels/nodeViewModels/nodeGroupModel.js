import { types } from 'mobx-state-tree';
const { optional, array, string, model } = types;
import { NodeTypeModel } from './nodeTypeModel';

export const NodeGroupModel = model('NodeGroupModel', {
	code: string,
	name: string,
	nodeTypes: optional(array(NodeTypeModel), [])
});
