import { types } from 'mobx-state-tree';
const { optional, array, string, model, reference } = types;
import { ParamType } from './../../paramsModels/paramType';

export const NodeTypeModel = model('NodeTypeModel', {
	code: string,
	name: string,
	paramTypeCodes: optional(array(reference(ParamType)), [])
});
