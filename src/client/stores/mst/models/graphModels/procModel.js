import { types, getParent } from 'mobx-state-tree';
import { extendObservable } from 'mobx';
const { optional, maybe, array, string, number, model } = types;
import { LinkModel } from './linkModel';
import { NodeModel } from './nodeModel';
import { recorder } from './../../rootStore';
import _ from 'lodash';
import { isEmpty } from './../../helpers';
import { toJS } from 'mobx';

export const ProcModel = model('ProcModel', {
	id: maybe(string),
	nodes: optional(array(NodeModel), []),
	links: optional(array(LinkModel), []),
	offsetX: maybe(number),
	offsetY: maybe(number),
	zoom: optional(number, 100),
	gridSize: optional(number, 0),
	name: optional(string, '')
}).actions(self => {
	function setNewName(newName) {
		recorder.replaying = false;
		self.name = newName;
		recorder.replaying = true;
	}

	function updateModel(newModel) {
		// console.log('newModel', newModel);
		extendObservable(self, { ...newModel });
	}

	function onNodeSelected() {
		let selectedNodeParamTypes = [];
		let selectedNode = self.nodes.find(node => node.selected === true);
		let findNodeIndex = self.nodes.findIndex(node => node.selected === true);
		if (selectedNode) {
			let selectedNodeType = findNodeType(selectedNode.nodeTypeCode);
			let paramTypeCodes = selectedNodeType.paramTypeCodes;
			if (isEmpty(toJS(selectedNode.params)) && isEmpty(toJS(self.nodes[findNodeIndex].params))) {
				selectedNodeParamTypes = generateParams(paramTypeCodes);
				self.nodes.forEach((value, index) => {
					if (index === findNodeIndex) {
						value.params.replace(selectedNodeParamTypes);
					}
				});
			}
		}
		// console.log('procModel', toJS(self));
	}

	function findNodeType(nodeTypeCode) {
		let nodeGroupsArray = [];
		let processStore = getParent(self);
		processStore.nodeViews.map(nv => nv.nodeGroups.map(ng => ng.nodeTypes.map(nt => nodeGroupsArray.push(nt))));
		return _.find(nodeGroupsArray, { code: nodeTypeCode });
	}
	function generateParams(paramTypeCodes) {
		return paramTypeCodes.map(paramTemp => {
			return { paramTypeCode: paramTemp.code, values: [] };
		});
	}
	return {
		setNewName,
		updateModel,
		onNodeSelected
	};
});
