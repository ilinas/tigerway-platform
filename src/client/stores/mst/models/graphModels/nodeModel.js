import { types } from 'mobx-state-tree';
import { PortModel } from './portModel';
const { optional, maybe, union, array, boolean, string, frozen, number, model, identifier } = types;

export const ParamModel = model('ParamModel', {
	id: maybe(number),
	paramTypeCode: string,
	values: optional(array(union(string, boolean, number)), [])
}).actions(self => ({
	setNewValue(values) {
		self.values.clear();
		if (values[0] === undefined) {
			self.values = [];
		}
		else {
			values.forEach((value, index) => {
				self.values.push(value);
			});
		}
		// console.log('ParamModel', self.values);
	}
}));

export const NodeModel = model('NodeModel', {
	extras: optional(frozen, {}),
	id: optional(identifier(string), ''),
	name: optional(string, ''),
	nodeTypeCode: optional(string, ''),
	ports: optional(array(PortModel), []),
	params: optional(array(ParamModel), []),
	selected: optional(boolean, false),
	type: optional(string, ''),
	x: maybe(number),
	y: maybe(number),
	_class: optional(string, '')
}).actions(self => ({}));
