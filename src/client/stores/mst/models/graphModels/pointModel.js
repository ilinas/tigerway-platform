import { types } from 'mobx-state-tree';
const { optional, maybe, boolean, string, number, model } = types;

export const PointModel = model('PointModel', {
	id: optional(string, ''),
	selected: optional(boolean, false),
	x: maybe(number),
	y: maybe(number),
	_class: optional(string, '')
});
