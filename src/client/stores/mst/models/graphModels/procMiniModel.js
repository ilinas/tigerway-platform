import { types } from 'mobx-state-tree';
const { optional, string, model, identifier } = types;

export const ProcMiniModel = model('ProcMiniModel', {
	id: optional(identifier(string), ''),
	name: optional(string, '')
	// model: optional(string, '')
});
