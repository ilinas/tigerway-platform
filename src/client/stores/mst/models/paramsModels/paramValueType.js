import { types } from 'mobx-state-tree';
const { optional, maybe, array, string, number, model, identifier } = types;

export const ParamValueType = model('ParamValueType', {
	id: maybe(number),
	code: optional(identifier(string), ''),
	name: optional(string, ''),
	validationTypeCodes: optional(array(string), [])
});
