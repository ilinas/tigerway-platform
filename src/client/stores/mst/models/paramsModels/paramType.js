import { types } from 'mobx-state-tree';
const { optional, array, string, number, model, identifier } = types;
import { ParamOption } from './paramOption';

export const ParamType = model('ParamType', {
	id: number,
	name: optional(string, ''),
	code: identifier(string),
	paramOptions: optional(array(ParamOption), []),
	paramValueTypeCode: optional(string, '')
	// validation: optional(ParamTypeValidationModel , {})
});
