import { types } from 'mobx-state-tree';
const { optional, maybe, string, number, model } = types;

export const ParamOption = model('ParamOption', {
	id: maybe(number),
	value: optional(string, '')
});
