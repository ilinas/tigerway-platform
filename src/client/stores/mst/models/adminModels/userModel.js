import { types } from 'mobx-state-tree';
const { optional, number, maybe, string, array, model, identifier } = types;

export const UserModel = model('UserModel', {
	id: optional(identifier(number), 0),
	number: optional(number, 0),
	companyName: maybe(string),
	plainPassword: maybe(string),
	email: maybe(string),
	roles: maybe(array(string))
}).actions(self => ({
	changeData(field, data) {
		self[field] = data;
	}
}));
