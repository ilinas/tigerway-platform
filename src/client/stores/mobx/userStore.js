import { observable, extendObservable, action, toJS } from 'mobx';
import baseApi from './../../api/baseApi';
import { loginApi } from './../../api/loginApi';

class UserStore {
	@observable
	userData = {
		login: null,
		roles: [],
		token: null
	};
	@observable _isStoreInitialized = false;
	@observable isSessionCorrect = false;

	startStore() {
		let userData = localStorage.getItem('userData');

		if (userData) {
			this.setUserData(JSON.parse(userData));
			this.isSessionCorrect = true;
		}
		this._isStoreInitialized = true;
	}
	// methods
	@action
	setUserData(data) {
		extendObservable(this.userData, toJS(data));
		baseApi.setAuthorizationHeader(this.userData.token);
		localStorage.setItem('userData', JSON.stringify(this.userData));
	}

	loginUser({ login, password }) {
		return loginApi.doLogIn(login, password);
	}

	getUserData() {
		return this.userData;
	}

	resetUserData() {
		extendObservable(this.userData, {
			login: null,
			roles: [],
			token: null
		});

		localStorage.removeItem('userData');
	}

	get isLoggedIn() {
		return !!this.userData.token;
	}
	get isInitialized() {
		return this._isStoreInitialized;
	}
}

export default new UserStore();
