// lib
import React, { Component } from 'react';
import { Timeline, Row, Col, Card, Calendar, Icon } from 'antd';
import ru from 'antd/lib/locale-provider/ru_RU';
import moment from 'moment';
// src
import './HomePage.scss';
import { setInterval } from 'timers';

export default class Home extends Component {
	state = {
		time: moment()
	};
	componentDidMount() {
		setInterval(() => {
			this.setState({ time: moment() });
		}, 1000);
	}
	render() {
		return (
			<div className="home-page w100">
				<Row gutter={16}>
					<Col xs={24} sm={24} md={24} lg={8}>
						<Card>
							<Timeline>
								<Timeline.Item>Создан новый процесс 2015-09-01</Timeline.Item>
								<Timeline.Item color="blue">Создан новый процесс 2015-09-01</Timeline.Item>
								<Timeline.Item color="red">
									<p>Не удалось выполнить процесс 1</p>
									<p>Не удалось выполнить процесс 2</p>
									<p>Не удалось выполнить процесс 3 2015-09-01</p>
								</Timeline.Item>
								<Timeline.Item
									dot={<Icon type="clock-circle-o" style={{ fontSize: '16px' }} />}
									color="red">
									Технические работы 2015-09-01
								</Timeline.Item>
								<Timeline.Item color="green">
									<p>Выпонен процесс 1</p>
									<p>Выпонен процесс 2</p>
									<p>Выпонен процесс 3 2015-09-01</p>
								</Timeline.Item>
							</Timeline>
						</Card>
					</Col>
					<Col xs={24} sm={24} md={24} lg={8}>
						<Card>
							<h3>{this.state.time.format('HH:mm:ss')}</h3>
						</Card>
					</Col>
					<Col xs={24} sm={24} md={24} lg={8}>
						<Card>
							<Calendar locale={ru} fullscreen={false} onPanelChange={this.onPanelChange} />
						</Card>
					</Col>
				</Row>
			</div>
		);
	}
}
