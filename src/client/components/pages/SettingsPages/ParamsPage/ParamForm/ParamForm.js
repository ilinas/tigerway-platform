import React, { Component } from 'react';
import { Form, Input, Button, Row, Col, Select, Popconfirm, Icon } from 'antd';
import { observer, inject } from 'mobx-react';
import { INPUTTYPES } from './../../../../../constants/inputTypesConstant';
const FormItem = Form.Item;
const Option = Select.Option;
import './ParamForm.scss';
import ValidationForm from './../../../../common/ValidationForm';

@inject('processStore', 'paramsStore', 'validationStore')
@observer
class ParamForm extends Component {
	state = {
		showMulti: false,
		expandValidation: false
	};
	componentDidMount() {
		if (this.props.initialParamTypeValue) {
			this.setShowMultiFlag(this.props.initialParamTypeValue.paramValueTypeCode);
			this.props.paramsStore.setValidationsArray(this.props.paramsStore.paramValueTypes, this.props.initialParamTypeValue.paramValueTypeCode);
		}
	}
	handleSubmit = e => {
		const { mode, form, paramsStore } = this.props;
		e.preventDefault();
		form.validateFields((err, values) => {
			if (!err) {
				console.log('Received values of form: ', values);
				if (mode === 'create') {
					let res = paramsStore.createParam(values);
					if (res) {
						form.resetFields();
					}
				}
				else {
					let target = e.target[1];
					let id = target.dataset.id;
					paramsStore.updateParam(values, id);
				}
			}
		});
	};
	handleDeleteParam = id => {
		const { paramsStore } = this.props;
		paramsStore.deleteParam(id);
	};
	setShowMultiFlag(code) {
		let arrayParams = Object.values(INPUTTYPES);
		let result = arrayParams.find(param => param.code === code);
		this.setState({ showMulti: result.multi });
	}
	selectChangeHandler = e => {
		let code = e;
		this.props.paramsStore.setValidationsArray(this.props.paramsStore.paramValueTypes, code);
		this.setShowMultiFlag(code);
	};
	toggleValidation = () => {
		const { expandValidation } = this.state;
		this.setState({ expandValidation: !expandValidation });
	};
	render() {
		const { getFieldDecorator } = this.props.form;
		const { initialParamTypeValue, initialParamTypeOptionsValue, initialName, mode, paramId, form } = this.props;
		const { showMulti, expandValidation } = this.state;
		let paramValueTypeCode = initialParamTypeValue ? initialParamTypeValue.paramValueTypeCode : '';
		let initialValidationState = initialParamTypeValue ? initialParamTypeValue.validation.validationRules : [];
		return (
			<Form onSubmit={this.handleSubmit} className="param-form">
				<Row gutter={40}>
					<Col span={8}>
						<FormItem>
							{getFieldDecorator('paramValueTypeCode', { initialValue: paramValueTypeCode })(
								<Select placeholder="Выберете тип" onChange={this.selectChangeHandler}>
									{Object.values(INPUTTYPES).map(type => (
										<Option key={type.code} value={type.code}>
											{type.name}
										</Option>
									))}
								</Select>
							)}
						</FormItem>
					</Col>
					{showMulti && (
						<Col span={16}>
							<FormItem>
								{getFieldDecorator('paramOptions', { initialValue: initialParamTypeOptionsValue.length > 0 ? initialParamTypeOptionsValue.map(option => `${option.id}`) : [] })(
									<Select mode="tags" placeholder="Введите имя">
										{initialParamTypeOptionsValue.length > 0
											? initialParamTypeOptionsValue.map(optionValue => (
												<Option key={optionValue.id} value={`${optionValue.id}`}>
													{optionValue.value}
												</Option>
												))
											: []}
									</Select>
								)}
							</FormItem>
						</Col>
					)}
				</Row>
				<Row gutter={40}>
					<Col span={8}>
						<FormItem>{getFieldDecorator('name', { initialValue: initialName })(<Input placeholder="Введите имя" />)}</FormItem>
					</Col>
					<Col span={16} style={{ textAlign: 'right' }}>
						<a style={{ marginRight: 8, fontSize: 14 }} onClick={this.toggleValidation}>
							Валидация <Icon type={expandValidation ? 'up' : 'down'} />
						</a>
						<Button data-id={paramId} type="primary" htmlType="submit" size={'large'}>
							{mode === 'create' ? 'Сохранить' : 'Обновить'}
						</Button>
						{mode !== 'create' && (
							<Popconfirm placement="topRight" title={'Вы точно хотите удалить этот параметр?'} onConfirm={() => this.handleDeleteParam(paramId)} okText="Да" cancelText="Нет">
								<Button className="param-form__btn-delete" type="danger" htmlType="submit" size={'large'}>
									Удалить
								</Button>
							</Popconfirm>
						)}
					</Col>
				</Row>
				<Row gutter={40} className={expandValidation ? '' : 'hide'}>
					<Col span={24}>
						<ValidationForm initialValidationState={initialValidationState} form={form} mode={'groups1'} validationTypesCodes={this.props.validationStore.validationTypesCodes} />
					</Col>
				</Row>
			</Form>
		);
	}
}

export default Form.create()(ParamForm);
