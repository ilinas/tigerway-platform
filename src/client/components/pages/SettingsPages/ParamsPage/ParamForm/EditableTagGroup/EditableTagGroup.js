import React, { Component } from 'react';
import { Tag, Input, Tooltip, Button, Icon } from 'antd';

class TagCustom extends Component {
	state = {
		name: '',
		isEditable: false
	};
	componentDidMount() {
		this.setState({ name: this.props.title });
	}
	handleInputChange = e => {
		this.setState({ name: e.target.value });
	};
	handleInputConfirm = () => {
		const state = this.state;
		const name = state.name;
		this.setState({
			isEditable: false,
			name
		});
	};
	render() {
		const { name, isEditable } = this.state;
		return (
			<div>
				{!isEditable && (
					<div>
						{name}
						<Icon type="edit" onClick={() => this.setState({ isEditable: !isEditable })} />
					</div>
				)}
				{isEditable && <Input ref={this.saveInputRef} type="text" size="small" style={{ width: 78 }} value={name} onChange={this.handleInputChange} onBlur={this.handleInputConfirm} onPressEnter={this.handleInputConfirm} />}
			</div>
		);
	}
}

export default class EditableTagGroup extends Component {
	state = {
		tags: ['Unremovable', 'Tag 2', 'Tag 3'],
		inputVisible: false,
		inputValue: ''
	};

	handleClose = removedTag => {
		const tags = this.state.tags.filter(tag => tag !== removedTag);
		console.log(tags);
		this.setState({ tags });
	};

	handleInputChange = e => {
		this.setState({ inputValue: e.target.value });
	};

	handleInputConfirm = () => {
		const state = this.state;
		const inputValue = state.inputValue;
		let tags = state.tags;
		if (inputValue && tags.indexOf(inputValue) === -1) {
			tags = [...tags, inputValue];
		}
		console.log(tags);
		this.setState({
			tags,
			inputVisible: false,
			inputValue: ''
		});
	};
	showInput = () => {
		this.setState({ inputVisible: true }, () => this.input.focus());
	};

	saveInputRef = input => (this.input = input);

	render() {
		const { tags, inputVisible, inputValue } = this.state;
		return (
			<div>
				{tags.map(tag => {
					const isLongTag = tag.length > 10;
					const tagElem = (
						<Tag key={tag}>
							<TagCustom title={isLongTag ? `${tag.slice(0, 10)}...` : tag} />
						</Tag>
					);
					return isLongTag ? <Tooltip title={tag}>{tagElem}</Tooltip> : tagElem;
				})}
				{inputVisible && <Input ref={this.saveInputRef} type="text" size="small" style={{ width: 78 }} value={inputValue} onChange={this.handleInputChange} onBlur={this.handleInputConfirm} onPressEnter={this.handleInputConfirm} />}
				{!inputVisible && (
					<Button size="small" type="dashed" onClick={this.showInput}>
						+ New Tag
					</Button>
				)}
			</div>
		);
	}
}
