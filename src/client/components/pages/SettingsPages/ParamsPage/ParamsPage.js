// libs
import React, { Component } from 'react';
import cn from 'cn-decorator';
import { observer, inject } from 'mobx-react';
import { Collapse, Spin } from 'antd';
const Panel = Collapse.Panel;
// src
import ParamForm from './ParamForm';
import './ParamsPage.scss';

@inject('processStore', 'paramsStore')
@observer
@cn('params-page')
export default class ParamsPage extends Component {
	onSave(values) {
		this.props.paramsStore.createParam(values);
	}
	render(cn) {
		const { paramsStore } = this.props;
		const { paramTypes } = this.props.paramsStore;
		return (
			<div className={cn()}>
				<h1 className={cn('title')}>Параметры</h1>
				<div className={cn('param-wrapper')}>
					<Collapse bordered={true}>
						<Panel header="Добавить параметр" key="0">
							<ParamForm initialParamTypeOptionsValue={[]} initialName={''} mode={'create'} />
						</Panel>
					</Collapse>
				</div>
				<Spin spinning={paramsStore.spin} size="large">
					{paramTypes.map((paramType, index) => (
						<div key={index} className={cn('param-wrapper')}>
							<Collapse bordered={true}>
								<Panel header={paramType.name} key={index}>
									<ParamForm paramId={paramType.id} initialParamTypeValue={paramType} initialParamTypeOptionsValue={paramType.paramOptions} initialName={paramType.name} mode={'update'} />
								</Panel>
							</Collapse>
						</div>
					))}
				</Spin>
			</div>
		);
	}
}
