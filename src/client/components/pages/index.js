export {default as AboutPage} from './AboutPage';
export {default as ProcessPage} from './ProcessPage';
export {default as HomePage} from './HomePage';
export {default as NotFoundPage} from './NotFoundPage';
