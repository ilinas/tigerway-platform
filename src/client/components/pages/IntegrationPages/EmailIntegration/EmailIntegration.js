import React, { Component } from 'react';
import './EmailIntegration.scss';
import { inject, observer } from 'mobx-react';
import { Form, Button, Input, Select, Spin, Popconfirm } from 'antd';
const FormItem = Form.Item;
const Option = Select.Option;

@Form.create()
@inject('integrationEmailStore')
@observer
export default class EmailIntegration extends Component {
	componentWillMount() {
		this.props.integrationEmailStore.getEmailData();
	}
	handleSubmit = e => {
		e.preventDefault();
		const { integrationEmailStore, form } = this.props;
		form.validateFields((err, values) => {
			if (!err) {
				integrationEmailStore.setEmailData(values);
			}
		});
	};
	clearData = () => {
		const { integrationEmailStore, form } = this.props;
		integrationEmailStore.clearEmailData();
		form.resetFields();
	};
	render() {
		const { getFieldDecorator } = this.props.form;
		let { ingData, isLoading } = this.props.integrationEmailStore;
		return (
			<div className="mail-integration__wrapper">
				<h2>Форма интеграции с почтой</h2>
				<Spin size="large" spinning={isLoading}>
					<Form layout="vertical" onSubmit={this.handleSubmit}>
						<FormItem label="Email">
							{getFieldDecorator('username', {
								initialValue: ingData.username || '',
								rules: [
									{ required: true, message: 'Поле email обязательно!' },
									{ type: 'email', message: 'Поле email не соответствует email!' }
								]
							})(<Input placeholder="Email" />)}
						</FormItem>
						<FormItem label="Пароль">
							{getFieldDecorator('password', {
								initialValue: ingData.password || '',
								rules: [{ required: true, message: 'Поле пароль обязательно!' }]
							})(<Input placeholder="Пароль" />)}
						</FormItem>
						<FormItem label="Используемый сервис">
							{getFieldDecorator('server', {
								initialValue: ingData.server || '',
								rules: [{ required: true, message: 'Поле используемый сервис обязательно!' }]
							})(
								<Select allowClear={true}>
									<Option value="smtp.yandex.ru">Yandex</Option>
									<Option value="smtp.gmail.com">Google</Option>
									<Option value="smtp.mail.ru">Mail</Option>
								</Select>
							)}
						</FormItem>
						<FormItem>
							<Button type="primary" htmlType="submit">
								Сохранить
							</Button>
							<Popconfirm
								placement="topRight"
								title={'Вы точно хотите отвязать аккаунт?'}
								onConfirm={() => this.clearData()}
								okText="Да"
								cancelText="Нет">
								<Button
									className={'mail-integration__btn--remove'}
									type="danger"
									disabled={!ingData.username}
									htmlType="button">
									Отвязать аккаунт
								</Button>
							</Popconfirm>
						</FormItem>
					</Form>
				</Spin>
			</div>
		);
	}
}
