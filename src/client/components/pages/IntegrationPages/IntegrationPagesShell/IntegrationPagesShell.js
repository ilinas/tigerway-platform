import React, { Component } from 'react';

export default class IntegrationPagesShell extends Component {
	render() {
		return this.props.children;
	}
}