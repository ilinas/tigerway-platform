// lib
import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { Form, Button, Input, Tooltip, Spin, Popconfirm } from 'antd';
const FormItem = Form.Item;
import cn from 'cn-decorator';
// src
import './AmoIntegrationForm.scss';
@Form.create()
@inject('integrationAmoStore')
@observer
@cn('amo-form')
export default class AmoIntegrationForm extends Component {
	componentWillMount() {
		this.props.integrationAmoStore.getAmoData();
	}
	handleSubmit = e => {
		e.preventDefault();
		this.props.form.validateFields((err, values) => {
			if (!err) {
				if (this.props.integrationAmoStore.formValues.id) {
					this.props.integrationAmoStore.updateAmoData(values);
				}
				else {
					this.props.integrationAmoStore.saveAmoData(values);
				}
			}
		});
	};
	clearData = () => {
		this.props.integrationAmoStore.clearAmoData();
		this.props.form.resetFields();
	};
	refreshSchema = () => {
		this.props.integrationAmoStore.refreshAmoData();
	};
	render(cn) {
		const { getFieldDecorator } = this.props.form;
		let { formValues, isLoading, isRefreshSchema } = this.props.integrationAmoStore;
		return (
			<div className={cn()}>
				<h2>Форма интеграции с AMO</h2>
				<Spin size="large" spinning={isLoading}>
					<Form layout="vertical" onSubmit={this.handleSubmit}>
						<FormItem label="Email">
							<Tooltip placement="topRight" title="Введите логин вашего аккаунта в AMO CRM">
								{getFieldDecorator('login', { initialValue: formValues.login })(
									<Input placeholder="Email" />
								)}
							</Tooltip>
						</FormItem>
						<FormItem label="Поддомен">
							<Tooltip placement="topRight" title="Введите поддомен вашего аккаунта в AMO CRM">
								{getFieldDecorator('subdomain', { initialValue: formValues.subdomain })(
									<Input placeholder="ПодДомен" />
								)}
							</Tooltip>
						</FormItem>
						<FormItem label="Api ключ">
							<Tooltip placement="topRight" title="Введите ключ Api вашего аккаунта в AMO CRM">
								{getFieldDecorator('apikey', { initialValue: formValues.apikey })(
									<Input placeholder="Api ключ" />
								)}
							</Tooltip>
						</FormItem>
						<FormItem>
							<Button type="primary" htmlType="submit">
								{!!formValues.id ? 'Обновить' : 'Сохранить'}
							</Button>
							<Button
								className={'amo-form__btn'}
								icon={'sync'}
								htmlType="button"
								disabled={!formValues.id}
								loading={isRefreshSchema}
								onClick={this.refreshSchema}>
								Обновить схему
							</Button>
							<Popconfirm
								placement="topRight"
								title={'Вы точно хотите отвязать аккаунт?'}
								onConfirm={() => this.clearData()}
								okText="Да"
								cancelText="Нет">
								<Button
									className={'amo-form__btn'}
									type="danger"
									disabled={!formValues.id}
									htmlType="button">
									Отвязать аккаунт
								</Button>
							</Popconfirm>
						</FormItem>
					</Form>
				</Spin>
			</div>
		);
	}
}
