// lib
import React, { Component } from 'react';
import { Row, Col, Modal } from 'antd';
import cn from 'cn-decorator';
// src
import './AmoIntegration.scss';
import AmoIntegrationForm from './AmoIntegrationForm';
import AmoHelperSlider from './AmoHelperSlider';
import PageHelper from './../../../common/PageHelper';

@cn('amo-page')
export default class AmoIntegration extends Component {
	state = {
		showModal: false
	};
	handleOk = e => {
		this.setState({
			showModal: false
		});
	};
	handleCancel = e => {
		this.setState({
			showModal: false
		});
	};
	showModal = () => {
		this.setState({
			showModal: true
		});
	};
	render(cn) {
		return (
			<div className={cn()}>
				<PageHelper onClick={this.showModal}/>
				{/* <Row className={cn('row')} gutter={16}> */}
				{/* <Col className={cn('form-wrapper')} xs={24} sm={24} md={24} lg={12} xl={12}> */}
				<AmoIntegrationForm />
				{/* </Col> */}
				<Modal
					title="Помощь"
					visible={this.state.showModal}
					onOk={this.handleOk}
					width={900}
					wrapClassName={'slider-modal__wrapper'}
					onCancel={this.handleCancel}>
					<AmoHelperSlider />
				</Modal>
				{/* </Row> */}
			</div>
		);
	}
}
