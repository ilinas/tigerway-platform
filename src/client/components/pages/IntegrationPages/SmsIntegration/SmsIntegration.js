import React, { Component } from 'react';
import './SmsIntegration.scss';
import { inject, observer } from 'mobx-react';
import { Form, Button, Input, Select, Spin, Popconfirm } from 'antd';
const FormItem = Form.Item;
const Option = Select.Option;

@Form.create()
@inject('integrationSmsStore')
@observer
export default class SmsIntegration extends Component {
	componentWillMount() {
		this.props.integrationSmsStore.getSmsData();
	}
	handleSubmit = e => {
		e.preventDefault();
		const { integrationSmsStore, form } = this.props;
		form.validateFields((err, values) => {
			if (!err) {
				integrationSmsStore.setSmsData(values);
			}
		});
	};
	clearData = () => {
		const { integrationSmsStore, form } = this.props;
		integrationSmsStore.clearSmsData();
		form.resetFields();
	};
	render() {
		const { getFieldDecorator } = this.props.form;
		let { ingData, isLoading } = this.props.integrationSmsStore;
		return (
			<div className="sms-integration__wrapper">
				<h2>Форма интеграции с sms-сервисом</h2>
				<Spin size="large" spinning={isLoading}>
					<Form layout="vertical" onSubmit={this.handleSubmit}>
						<FormItem label="Логин">
							{getFieldDecorator('username', {
								initialValue: ingData.username || '',
								rules: [
									{ required: true, message: 'Поле логин обязательно!' }
								]
							})(<Input placeholder="Логин" />)}
						</FormItem>
						<FormItem label="Ключ">
							{getFieldDecorator('apikey', {
								initialValue: ingData.apikey || '',
								rules: [{ required: true, message: 'Поле ключ обязательно!' }]
							})(<Input placeholder="Ключ" />)}
						</FormItem>
						<FormItem label="Используемый сервис">
							{getFieldDecorator('service', {
								initialValue: ingData.service || '',
								rules: [{ required: true, message: 'Поле используемый сервис обязательно!' }]
							})(
								<Select allowClear={true}>
									<Option value="smsc.ru">smsc.ru</Option>
									<Option value="sms.ru">sms.ru</Option>
								</Select>
							)}
						</FormItem>
						<FormItem>
							<Button type="primary" htmlType="submit">
								Сохранить
							</Button>
							<Popconfirm
								placement="topRight"
								title={'Вы точно хотите отвязать аккаунт?'}
								onConfirm={() => this.clearData()}
								okText="Да"
								cancelText="Нет">
								<Button
									className={'sms-integration__btn--remove'}
									type="danger"
									disabled={!ingData.username}
									htmlType="button">
									Отвязать аккаунт
								</Button>
							</Popconfirm>
						</FormItem>
					</Form>
				</Spin>
			</div>
		);
	}
}
