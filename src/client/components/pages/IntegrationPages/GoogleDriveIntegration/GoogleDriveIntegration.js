import React, { Component } from 'react';
import './GoogleDriveIntegration.scss';
import DevPage from './../../SystemPages/DevPage';

export default class GoogleDriveIntegration extends Component {
	state = {
		show: false
	};
	componentDidMount() {
		this.setState({ show: true });
	}
	render() {
		return (
			<div className="google-integration__wrapper">
				<DevPage />
			</div>
		);
	}
}
