import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import './SiteIntegration.scss';

@inject('integrationSiteStore')
@observer
export default class SiteIntegration extends Component {
	componentDidMount() {
		this.props.integrationSiteStore.getApiKeyData();
	}
	getScriptCode() {
		return `<script type="text/javascript" >
	(function (d, w) {
            // configs
            window.onload = function () {
                try {
                    // methods
                    tigerCore.init(${this.props.integrationSiteStore.apiKey});
                } catch (e) {
                }
            };
        // add tigerCore    
        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "http://jslib.tigerway.ru/tigerCore.js";
        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    }) (document, window);
</script>`;
	}

	render() {
		return (
			<div className="site-integration__wrapper">
				<h1>Код интеграции</h1>
				<div className={'site-integration__code-wrapper'}>
					<span className={'site-integration__code'}>{this.getScriptCode()}</span>
				</div>
			</div>
		);
	}
}
