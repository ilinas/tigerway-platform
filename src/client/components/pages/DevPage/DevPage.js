import React, { Component } from 'react';
import './DevPage.scss';
import TemplateEditor from './../../common/TemplateEditor';

export default class DevPage extends Component {
	render() {
		return (
			<div className="dev-page">
				<h1>Welcome to DevPage</h1>
				<TemplateEditor defaultValue={'demo1'}/>
				<TemplateEditor defaultValue={'demo2'}/>
			</div>
		);
	}
}
