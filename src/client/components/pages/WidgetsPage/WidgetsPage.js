import React, { Component } from 'react';
import './WidgetsPage.scss';
import DevPage from './../SystemPages/DevPage';

export default class WidgetsPage extends Component {
	render() {
		return (
			<div className="widgets-page">
				<DevPage />
			</div>
		);
	}
}
