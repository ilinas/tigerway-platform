import React, { Component } from 'react';
import { Popover } from 'antd';
import PageHelper from './../../../../common/PageHelper';

export default class EditProcHelper extends Component {
	render() {
		const text = <span>Помощь по диаграмме</span>;
		const content = (
			<div>
				<p>Delete - удаляет выделенные объекты, кроме нод</p>
				<p>Shift + Mouse click - выделяет объект</p>
				<p>Mouse Drag позволяет перемещать рабочую область</p>
				<p>Mouse wheel регулирует зум</p>
				<p>Click link + Drag - создаст новую точку</p>
				<p>Click Node Port + Drag - создаст новую линку</p>
			</div>
		);
		return (
			<Popover placement="rightTop" title={text} content={content} trigger="click">
				<PageHelper />
			</Popover>
		);
	}
}
