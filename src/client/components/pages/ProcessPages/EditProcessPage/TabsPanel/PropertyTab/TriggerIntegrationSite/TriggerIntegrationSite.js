import React, { Component } from 'react';
import './TriggerIntegrationSite.scss';
import { Select, Checkbox, Button, message, Cascader } from 'antd';
const Option = Select.Option;
import CopyToClipboard from 'react-copy-to-clipboard';

const success = () => {
	message.success('Код успешно скопирован');
};

export default class TriggerIntegrationSite extends Component {
	state = {
		isOneStringText: false,
		copyValue: '',
		selectedCascaderParams: [],
		selectedCodeParams: []
	};
	componentDidMount() {
		let copyValue = this.getScriptCode();
		this.setState({ copyValue });
	}
	handleChange = async value => {
		let selectedCodeParams = [];
		this.props.paramsStore.paramTypes.forEach(paramType => {
			value.forEach(selectedCodeParam => {
				if (selectedCodeParam === paramType.code) {
					selectedCodeParams.push(paramType);
				}
			});
		});
		this.setState({ selectedCodeParams });
		await this.getSelectedCascaderParams(value);
		this.getFormattedCode(this.state.isOneStringText);
	};
	getParam(param) {
		return `${param.code}: "${param.name}",\n\t\t\t\t`;
	}
	getScriptCode() {
		return `<script type="text/javascript" >
	(function () {
		 window.TIGERCORE.modules.main.init();
	  	 $('#loginForm').submit(function(e) {
             e.preventDefault();
             window.TIGERCORE.modules.main.sendForm({params: {
				 ${this.state.selectedCodeParams.map(param => this.getParam(param)).join('')}}});
      });
     };
	var n = d.getElementsByTagName("script")[0],
			s = d.createElement("script"),
			f = function () {n.parentNode.insertBefore(s, n); }; 
		s.type = "text/javascript";
		s.async = true;
		s.src = "https://mc.yandex.ru/metrika/tag.js";
		if (w.opera == "[object Opera]") {
		d.addEventListener("DOMContentLoaded", f, false);
	} else {f(); } 
	})();
</script>`;
	}
	getSelectedCascaderParams(selectedCascaderParams) {
		let params = [];
		let formattedParams = [];
		this.props.paramsStore.paramTypes.forEach(paramType => {
			selectedCascaderParams.forEach(selectedParam => {
				if (selectedParam === paramType.code && paramType.paramOptions.length > 0) {
					params.push(paramType);
				}
			});
		});
		formattedParams = params.map(param => {
			return {
				label: param.name,
				value: param.code,
				children: param.paramOptions.map(paramOption => {
					return {
						label: paramOption.value,
						value: paramOption.id,
						id: paramOption.id
					};
				})
			};
		});
		this.setState({ selectedCascaderParams: formattedParams });
	}

	getFormattedCode(param) {
		let copyValue = this.getScriptCode();
		if (param) {
			copyValue = copyValue.replace(/[\s]+/g, ' ');
		}
		this.setState({ copyValue });
	}
	getClass() {
		return this.state.isOneStringText ? '' : 'no-one-string';
	}
	onOneLineChange = () => {
		let isOneStringText = this.state.isOneStringText;
		this.setState({ isOneStringText: !isOneStringText });
		this.getFormattedCode(!isOneStringText);
	};
	onCopy = () => {
		success();
	};
	onCascaderChange = (value, selectedOptions) => {
		this.setState({
			text: `${selectedOptions.map(o => o.label).join(', ')} id: ${value[1]}`
		});
	};
	render() {
		return (
			<div className="trigger-integration">
				<h1>Код интеграции</h1>
				<Select
					className="trigger-integration__select-params"
					mode="multiple"
					style={{ width: '100%' }}
					placeholder="Выберете параметры"
					onChange={this.handleChange}>
					{this.props.paramsStore.paramTypes.map(item => (
						<Option key={item.code} value={item.code}>
							{item.name}
						</Option>
					))}
				</Select>

				<p className="trigger-integration__code-description">
					Подключите данный код на странице и запустите функцию в момент отправки пользователем.
				</p>
				{this.state.selectedCascaderParams.length > 0 && (
					<Cascader
						style={{ width: '50%' }}
						showSearch={true}
						className={'custom-cascader'}
						options={this.state.selectedCascaderParams}
						onChange={this.onCascaderChange}
						notFoundContent={'Не найдено'}/>
				)}
				{this.state.text}
				<div className="trigger-integration__code-editor">
					<div className="code-editor__controls">
						<Checkbox
							value={this.state.isOneStringText}
							className="code-editor__control-item"
							onChange={this.onOneLineChange}>
							В одну строку
						</Checkbox>
					</div>
					<div className="trigger-integration__code-wrapper">
						<CopyToClipboard text={this.state.copyValue} onCopy={this.onCopy}>
							<Button className="btn-copy" size="large" shape="circle" icon="copy" />
						</CopyToClipboard>
						<span className={`trigger-integration__code ${this.getClass()} `}>{this.getScriptCode()}</span>
					</div>
				</div>
			</div>
		);
	}
}
