import React from 'react';
import { observer, inject } from 'mobx-react';
import { Modal, Button } from 'antd';
import TriggerIntegrationSite from './TriggerIntegrationSite/TriggerIntegrationSite';
import NodeParamsForm from './NodeParamsForm';
import Card from './../../../../../common/Card';
import './PropertyTab.scss';

@inject('processStore', 'paramsStore')
@observer
export class PropertyTab extends React.Component {
	info = () => {
		Modal.info({
			width: 900,
			maskClosable: true,
			content: <TriggerIntegrationSite paramsStore={this.props.paramsStore} />,
			onOk() {}
		});
	};
	onNodeSelected = () => {
		this.props.processStore.model.onNodeSelected();
	};
	render() {
		// let selectedNode = this.props.processStore.model.nodes.find(node => node.selected === true);
		let selectedNode = this.props.processStore.getSelectedNode;
		this.onNodeSelected();
		return (
			<div className="property-tab">
				<Card title={selectedNode && `Параметры блока: ${selectedNode.name}`} mode={'params'}>
					{selectedNode ? (
						<div className="property-tab__wrapper">
							{selectedNode.type === 'trigger' && <div className="property-tab__controls">
								<Button onClick={this.info} className="property-tab__btn--trigger">Код интеграции</Button>
							</div>}
							<div className="property-tab__content">
								{selectedNode.params.length > 0 ? (
									<NodeParamsForm setModel={this.props.setModel} selectedNode={selectedNode} />
								) : (
									<h3 className="property-tab__head--indent">У выбранного блока нет параметров</h3>
								)}
							</div>
						</div>
					) : (
						<h3 className="property-tab__head--indent">Выберите блок</h3>
					)}
				</Card>
			</div>
		);
	}
}
