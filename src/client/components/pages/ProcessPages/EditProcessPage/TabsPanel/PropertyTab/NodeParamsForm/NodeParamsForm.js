// libs
import React from 'react';
import { Form } from 'antd';
import { observer, inject } from 'mobx-react';

// src
import ParamTypeSwitch from './../../../../../../common/ParamTypeSwitch';

@Form.create()
@inject('processStore', 'paramsStore')
@observer
export default class NodeParamsForm extends React.Component {
	getParamType(code) {
		return this.props.paramsStore.paramTypes.find(paramType => paramType.code === code);
	}
	setModel = () => {
		const { processStore, setModel } = this.props;
		// console.log('NodeParamsForm procModel', processStore.model.toJSON());
		setModel(processStore.model);
	};
	render() {
		let paramType = {};
		const { selectedNode } = this.props;
		return (
			<Form layout="vertical" onMouseLeave={this.setModel}>
				{selectedNode.params.map(param => {
					paramType = this.getParamType(param.paramTypeCode);
					return (
						<Form.Item key={paramType.name} label={paramType.name}>
							<ParamTypeSwitch param={param} paramType={paramType} />
						</Form.Item>
					);
				})}
			</Form>
		);
	}
}
