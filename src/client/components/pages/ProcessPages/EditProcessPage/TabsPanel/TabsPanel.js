// libs
import React, { Component } from 'react';
import { Tabs } from 'antd';
const TabPane = Tabs.TabPane;
// src
import { BlocksTab } from './BlocksTab/BlocksTab';
import { PropertyTab } from './PropertyTab/PropertyTab';
import './TabsPanel.scss';

export default class TabsPanel extends Component {
	render() {
		return (
			<Tabs className="tabs--wrapper" type="card">
				<TabPane tab="Добавить блок" key="1">
					<BlocksTab />
				</TabPane>
				<TabPane tab="Свойства" key="2">
					<PropertyTab diagramModel={this.props.diagramModel} setModel={this.props.setModel} />
				</TabPane>
			</Tabs>
		);
	}
}
