// libs
import React from 'react';
import { observer, inject } from 'mobx-react';
// src
import Node from './../../../DiagramComponents/Node';
import Accordion from './../../../../../../common/Accordion';
import Card from './../../../../../../common/Card';
import './NodesPanel.scss';

@inject('processStore')
@observer
export default class NodesPanel extends React.Component {
	render() {
		const { nodeViews } = this.props.processStore;
		return (
			<div className="nodes-panel">
				{nodeViews.length > 0 &&
					nodeViews.map(nodeView => (
						<div className="card__wrapper"  key={nodeView.code}>
							<Card title={nodeView.name} mode={'list'}>
								{nodeView.nodeGroups.map(group => (
									<Accordion key={group.code} isChild={true} title={group.name} code={group.code}>
										<div className="nodes-panel--editor">
											{group.nodeTypes.map(nodeType => (
												<div className="node-wrapper" key={nodeType.code}>
													<Node
														type={nodeView.code}
														node={{ name: nodeType.name, code: nodeType.code, params: [] }}/>
												</div>
											))}
										</div>
									</Accordion>
								))}
							</Card>
						</div>
					))}
			</div>
		);
	}
}
