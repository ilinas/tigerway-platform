import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import { TreeSelect } from 'antd';
const TreeNode = TreeSelect.TreeNode;

import Card from './../../../../../common/Card';
import Node from './../../DiagramComponents/Node';
import NodesPanel from './NodesPanel';
import './NodesPanel/NodesPanel.scss';
import './BlocksTab.scss';

@inject('processStore')
@observer
export class BlocksTab extends Component {
	state = {
		searchValue: undefined,
		searchResult: []
	};
	findText(items, text) {
		if (!items) {
			return;
		}
		for (let item of items) {
			if (item.name === text) {
				let nodesArray = [];
				item.nodeGroups.map(group =>
					group.nodeTypes.map(type => nodesArray.push({ nodeType: type, parentType: item.code }))
				);
				return nodesArray;
			}
			if (!item.nodeGroups) {
				return;
			}
			for (let group of item.nodeGroups) {
				if (group.name === text) {
					return group.nodeTypes.map(type => {
						return { nodeType: type, parentType: item.code };
					});
				}
				if (!group.nodeTypes) {
					return;
				}
				for (let type of group.nodeTypes) {
					if (type.name === text) {
						return [{ nodeType: type, parentType: item.code }];
					}
				}
			}
		}
	}
	onSeachChangeHandler = value => {
		let searchResult = this.findText(this.props.processStore.nodeViews, value);
		this.setState({ searchValue: value });
		this.setState({ searchResult });
	};
	render() {
		const { nodeViews } = this.props.processStore;
		const { searchValue, searchResult } = this.state;
		return (
			nodeViews &&
			nodeViews.length > 0 && (
				<section className={'block-tab'}>
					<TreeSelect
						showSearch
						style={{ width: '100%' }}
						value={searchValue}
						dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
						placeholder="Выберите блок"
						allowClear
						onChange={this.onSeachChangeHandler}>
						{nodeViews.map(nodeView => (
							<TreeNode value={nodeView.name} title={nodeView.name} key={nodeView.code}>
								{nodeView.nodeGroups.map(group => (
									<TreeNode value={group.name} title={group.name} key={group.code}>
										{group.nodeTypes.map(nodeType => (
											<TreeNode value={nodeType.name} title={nodeType.name} key={nodeType.code} />
										))}
									</TreeNode>
								))}
							</TreeNode>
						))}
					</TreeSelect>
					<div className={searchValue !== undefined ? 'hide' : 'nodes-panel'}>
						<NodesPanel />
					</div>
					<div className={searchValue === undefined ? 'hide' : 'search-panel'}>
						<Card title={'Результаты поиска'} mode={'search'}>
							<div className = {'search-panel--editor'}>
								{searchResult !== undefined &&
									searchResult.length > 0 &&
									searchResult.map(result => (
										<div className="node-wrapper" key={result.nodeType.code}>
											<Node
												type={result.parentType}
												node={{
													name: result.nodeType.name,
													code: result.nodeType.code,
													params: result.nodeType.paramTypeCodes
												}}/>
										</div>
									))}
							</div>
						</Card>
					</div>
				</section>
			)
		);
	}
}
