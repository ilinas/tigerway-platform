// libs
import React from 'react';
import { DropTarget } from 'react-dnd';
import * as SRD from 'storm-react-diagrams';

// src
import { TriggerNodeModel } from './nodes/trigger/TriggerNodeModel';
import { ActionNodeModel } from './nodes/action/ActionNodeModel';
import { ConditionNodeModel } from './nodes/condition/ConditionNodeModel';
import { diagramEngine } from './Engine';
import './Diagram.scss';
// Setup the diagram model

const nodesTarget = {
	drop(props, monitor, component) {
		const { x: pageX, y: pageY } = monitor.getSourceClientOffset();
		const { left = 0, top = 0 } = props.diagramEngine.canvas.getBoundingClientRect();
		const { offsetX, offsetY } = props.diagramEngine.diagramModel;
		const x = pageX - left - offsetX;
		const y = pageY - top - offsetY;
		const item = monitor.getItem();
		let node;

		if (item.type === 'trigger') {
			node = new TriggerNodeModel(item.children.props.node);
		}
		else if (item.type === 'action') {
			node = new ActionNodeModel(item.children.props.node);
		}
		else {
			node = new ConditionNodeModel(item.children.props.node);
		}

		node.x = x;
		node.y = y;
		node.addListener({
			selectionChanged: selectedNode => {
				if (selectedNode.isSelected) {
					props.model.updateModel(props.diagramModel.serializeDiagram());
				}
			}
		});
		// props.diagramModel.addNode(node);
		// console.log('model', props.model.toJSON());
		props.model.nodes.push(node.serialize());
		// console.log('model after update', props.model.toJSON());
		props.setModel(props.model);
		// console.log('setModel', props.diagramEngine.diagramModel.serializeDiagram());
	}
};

@DropTarget('node-source', nodesTarget, (connect, monitor) => ({
	connectDropTarget: connect.dropTarget(),
	isOver: monitor.isOver(),
	canDrop: monitor.canDrop()
}))
export class Diagram extends React.Component {
	componentWillMount() {
		window.addEventListener('keyup', this.prevDelButtonOnNodes, false);
	}
	componentWillUnmount() {
		window.removeEventListener('keyup', this.prevDelButtonOnNodes);
	}
	setModel = () => {
		const { setModel, model, diagramEngine } = this.props;
		model.updateModel(diagramEngine.diagramModel.serializeDiagram());
		setModel(diagramEngine.diagramModel.serializeDiagram());
	};
	prevDelButtonOnNodes = e => {
		// console.log('prevDelButtonOnNodes');
		let selectedElem = this.props.diagramEngine.getDiagramModel().getSelectedItems()[0];
		if ([46].indexOf(e.keyCode) !== -1 && selectedElem instanceof SRD.NodeModel) {
			e.stopImmediatePropagation();
		}
	};
	render() {
		const { connectDropTarget } = this.props;
		// Render the canvas
		return connectDropTarget(
			<div className="diagram-drop-container" onMouseLeave={this.setModel}>
				<SRD.DiagramWidget deleteKeys={[46]} diagramEngine={diagramEngine} />
			</div>
		);
	}
}
