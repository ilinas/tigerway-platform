import React from "react";

import { DragWrapper } from "./DragWrapper";
import { TriggerNodeWidget } from "./nodes/trigger/TriggerNodeWidget";
import { ActionNodeWidget } from "./nodes/action/ActionNodeWidget";
import { ConditionNodeWidget } from "./nodes/condition/ConditionNodeWidget";
import { diagramEngine } from "./Engine";

export default class Node extends React.Component {
	renderNode() {
		const { type, node } = this.props;
		if (type === "trigger") {
			return <TriggerNodeWidget node={node} diagramEngine={diagramEngine} displayOnly />;
		}
		if (type === "action") {
			return <ActionNodeWidget node={node} diagramEngine={diagramEngine} displayOnly />;
		}
		if (type === "condition") {
			return <ConditionNodeWidget node={node} diagramEngine={diagramEngine} displayOnly />;
		}
		console.warn("Unknown node type");
		return null;
	}

	render() {
		const { type } = this.props;
		return <DragWrapper type={type}>{this.renderNode()}</DragWrapper>;
	}
}
