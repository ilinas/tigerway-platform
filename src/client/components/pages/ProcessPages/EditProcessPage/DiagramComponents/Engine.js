// libs
import React from 'react';
import * as SRD from 'storm-react-diagrams';

import { TriggerWidgetFactory } from './nodes/trigger/TriggerWidgetFactory';
// import { TriggerNodeFactory } from './nodes/trigger/TriggerInstanceFactories';
import { ActionWidgetFactory } from './nodes/action/ActionWidgetFactory';
// import { ActionNodeFactory } from './nodes/action/ActionInstanceFactories';
import { ConditionWidgetFactory } from './nodes/condition/ConditionWidgetFactory';
// import { ConditionNodeFactory } from './nodes/condition/ConditionInstanceFactories';

// Setup the diagram engine
export const diagramEngine = new SRD.DiagramEngine();
diagramEngine.installDefaultFactories();

// diagramEngine.registerNodeFactory(new SRD.DefaultNodeFactory());
// diagramEngine.registerLinkFactory(new SRD.DefaultLinkFactory());
diagramEngine.registerNodeFactory(new TriggerWidgetFactory());
diagramEngine.registerNodeFactory(new ActionWidgetFactory());
diagramEngine.registerNodeFactory(new ConditionWidgetFactory());

// Register instance factories
// diagramEngine.registerInstanceFactory(new SRD.DefaultNodeModel());
// diagramEngine.registerInstanceFactory(new SRD.DefaultPortInstanceFactory());
// diagramEngine.registerInstanceFactory(new SRD.LinkInstanceFactory());
// diagramEngine.registerInstanceFactory(new TriggerWidgetFactory());
// diagramEngine.registerInstanceFactory(new ActionWidgetFactory());
// diagramEngine.registerInstanceFactory(new ConditionWidgetFactory());
