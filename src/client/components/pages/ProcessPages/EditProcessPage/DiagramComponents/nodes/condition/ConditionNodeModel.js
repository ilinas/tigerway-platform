// libs
import _ from 'lodash';
import * as SRD from 'storm-react-diagrams';

export class ConditionNodeModel extends SRD.NodeModel {
	constructor(nodeData = {}) {
		super('condition');
		this.addPort(new SRD.DefaultPortModel(true, 'input', 'input'));
		this.addPort(new SRD.DefaultPortModel(false, 'yes', 'yes'));
		this.addPort(new SRD.DefaultPortModel(false, 'no', 'no'));
		this.name = nodeData.name || '';
		this.nodeTypeCode = nodeData.code || '';
		this.params = nodeData.params || [];
	}

	deSerialize(object) {
		super.deSerialize(object);
		this.name = object.name;
		this.nodeTypeCode = object.nodeTypeCode;
		this.params = object.params;
	}

	serialize() {
		return _.merge(super.serialize(), {
			name: this.name,
			nodeTypeCode: this.nodeTypeCode,
			params: this.params
		});
	}

	getInPort() {
		return this.ports.input;
	}

	getYesPort() {
		return this.ports.yes;
	}
	getNoPort() {
		return this.ports.no;
	}
}
