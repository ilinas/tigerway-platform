// libs
import * as SRD from 'storm-react-diagrams';
import React from 'react';
// src
import { TriggerNodeWidget } from './TriggerNodeWidget';
import { TriggerNodeModel } from './TriggerNodeModel';
const TriggerNodeWidgetFactory = React.createFactory(TriggerNodeWidget);

export class TriggerWidgetFactory extends SRD.NodeFactory {
	constructor() {
		super('trigger');
	}
	generateReactWidget(diagramEngine, node) {
		return TriggerNodeWidgetFactory({ node, diagramEngine });
	}
	getNewInstance() {
		return new TriggerNodeModel();
	}
}
