// libs
import React from 'react';
import * as SRD from 'storm-react-diagrams';
// src
import { ConditionNodeWidget } from './ConditionNodeWidget';
import { ConditionNodeModel } from './ConditionNodeModel';
const ConditionNodeWidgetFactory = React.createFactory(ConditionNodeWidget);

export class ConditionWidgetFactory extends SRD.NodeFactory {
	constructor() {
		super('condition');
	}

	generateReactWidget(diagramEngine, node) {
		return ConditionNodeWidgetFactory({ node, diagramEngine });
	}
	getNewInstance() {
		return new ConditionNodeModel();
	}
}
