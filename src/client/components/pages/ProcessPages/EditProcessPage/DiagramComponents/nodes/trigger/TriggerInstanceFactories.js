// src
import { AbstractInstanceFactory } from 'storm-react-diagrams';
import { TriggerNodeModel } from './TriggerNodeModel';

export class TriggerNodeFactory extends AbstractInstanceFactory {
	constructor() {
		super('TriggerNodeModel');
	}

	getInstance() {
		return new TriggerNodeModel();
	}
}
