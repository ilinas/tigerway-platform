// src

import { AbstractInstanceFactory } from "storm-react-diagrams";
import { ConditionNodeModel } from "./ConditionNodeModel";

export class ConditionNodeFactory extends AbstractInstanceFactory {
	constructor() {
		super("ConditionNodeModel");
	}

	getInstance() {
		return new ConditionNodeModel();
	}
}
