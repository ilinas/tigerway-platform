// src
import { AbstractInstanceFactory } from "storm-react-diagrams";
import { ActionNodeModel } from "./ActionNodeModel";

export class ActionNodeFactory extends AbstractInstanceFactory {
	constructor() {
		super("ActionNodeModel");
	}

	getInstance() {
		return new ActionNodeModel();
	}
}
