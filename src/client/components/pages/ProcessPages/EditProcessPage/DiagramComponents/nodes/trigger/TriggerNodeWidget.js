// libs
import React from 'react';
import * as SRD from 'storm-react-diagrams';
import { inject } from 'mobx-react';

// src
import { TriggerNodeModel } from './TriggerNodeModel';

@inject('processStore')
export class TriggerNodeWidget extends React.Component {
	getOutPorts() {
		const { node, color, displayOnly } = this.props;
		let outputNode = node;

		if (displayOnly) {
			outputNode = new TriggerNodeModel(node.name, color);
		}

		return outputNode.getOutPorts ? outputNode.getOutPorts().map((port, i) => <SRD.DefaultPortLabel model={port} key={`out-port-${i}`} />) : [];
	}
	onRemove() {
		const { node, diagramEngine, processStore } = this.props;
		node.remove();
		processStore.model.updateModel(diagramEngine.diagramModel.serializeDiagram());
		diagramEngine.repaintCanvas();
	}

	render() {
		const { node, displayOnly } = this.props;
		const { name } = node;

		return (
			<div className="basic-node__wrapper">
				<div className="basic-node">
					{!displayOnly ? <div className="basic-node__delete fa fa-close" onClick={this.onRemove.bind(this)} /> : null}
					<div className="title">
						<div className={!displayOnly ? 'name' : 'name ellipsis'} title={!displayOnly ? null : name}>
							{name}
						</div>
					</div>
					<div className="ports">
						<div className="out-node">{this.getOutPorts()}</div>
					</div>
				</div>
			</div>
		);
	}
}

// const TriggerNodeWidgetFactory = React.createFactory(TriggerNodeWidget);
// export default TriggerNodeWidgetFactory;
