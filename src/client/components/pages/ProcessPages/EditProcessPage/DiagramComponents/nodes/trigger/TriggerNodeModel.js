// libs
import _ from 'lodash';
import * as SRD from 'storm-react-diagrams';

export class TriggerNodeModel extends SRD.NodeModel {
	constructor(nodeData = {}) {
		super('trigger');
		this.addPort(new SRD.DefaultPortModel(false, 'output', 'output'));
		this.name = nodeData.name || '';
		this.nodeTypeCode = nodeData.code || '';
		this.params = nodeData.params || [];
	}

	deSerialize(object) {
		super.deSerialize(object);
		this.name = object.name;
		this.nodeTypeCode = object.nodeTypeCode;
		this.params = object.params;
	}

	serialize() {
		return _.merge(super.serialize(), {
			name: this.name,
			nodeTypeCode: this.nodeTypeCode,
			params: this.params
		});
	}

	getOutPorts() {
		return _.filter(this.ports, portModel => !portModel.in);
	}
}
