// libs
import _ from 'lodash';
import * as SRD from 'storm-react-diagrams';

export class ActionNodeModel extends SRD.NodeModel {
	constructor(nodeData = {}) {
		super('action');
		this.addPort(new SRD.DefaultPortModel(true, 'input', 'input'));
		this.addPort(new SRD.DefaultPortModel(false, 'output', 'output'));
		this.name = nodeData.name || '';
		this.nodeTypeCode = nodeData.code || '';
		this.params = nodeData.params || [];
	}

	deSerialize(object) {
		super.deSerialize(object);
		this.name = object.name;
		this.nodeTypeCode = object.nodeTypeCode;
		this.params = object.params;
	}

	serialize() {
		return _.merge(super.serialize(), {
			x: this.x,
			y: this.y,
			extras: this.extras,
			ports: _.map(this.ports, port => {
				return port.serialize();
			}),
			name: this.name,
			nodeTypeCode: this.nodeTypeCode,
			params: this.params
		});
	}

	getInPorts() {
		return this.ports.input;
	}
	getOutPorts() {
		return this.ports.output;
	}
}
