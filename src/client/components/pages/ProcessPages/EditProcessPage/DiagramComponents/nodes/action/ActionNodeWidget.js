// libs
import React from 'react';
import * as SRD from 'storm-react-diagrams';
import { inject } from 'mobx-react';
// src
import { ActionNodeModel } from './ActionNodeModel';

@inject('processStore')
export class ActionNodeWidget extends React.Component {
	getInPorts() {
		const { node, color, displayOnly } = this.props;
		let inputNode = node;

		if (displayOnly) {
			inputNode = new ActionNodeModel(node.name, color);
		}

		return inputNode.getInPorts ? <SRD.DefaultPortLabel model={inputNode.getInPorts()} /> : null;
	}
	getOutPorts() {
		const { node, color, displayOnly } = this.props;
		let inputNode = node;

		if (displayOnly) {
			inputNode = new ActionNodeModel(node.name, color);
		}

		return inputNode.getOutPorts ? <SRD.DefaultPortLabel model={inputNode.getOutPorts()} /> : null;
	}
	onRemove() {
		const { node, diagramEngine, processStore } = this.props;
		node.remove();
		processStore.model.updateModel(diagramEngine.diagramModel.serializeDiagram());
		diagramEngine.repaintCanvas();
	}

	render() {
		const { node, displayOnly } = this.props;
		const { name } = node;

		return (
			<div className="basic-node__wrapper">
				<div className="basic-node">
					<div className="input">
						<div className="dot-wrap">{this.getInPorts()}</div>
					</div>
					{!displayOnly ? (
						<div className="basic-node__delete fa fa-close" onClick={this.onRemove.bind(this)} />
					) : null}
					<div className="title">
						<div className={!displayOnly ? 'name' : 'name ellipsis'} title={!displayOnly ? null : name}>
							{name}
						</div>
					</div>
					<div className="ports">
						<div className="out-node">
							<div className="dot-wrap">{this.getOutPorts()}</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

// export const ActionNodeWidgetFactory = React.createFactory(ActionNodeWidget);
