// libs
import React from 'react';
import * as SRD from 'storm-react-diagrams';
import { inject } from 'mobx-react';
// src
import { ConditionNodeModel } from './ConditionNodeModel';

@inject('processStore')
export class ConditionNodeWidget extends React.Component {
	getInPort() {
		const { node, color, displayOnly } = this.props;
		let inputNode = node;

		if (displayOnly) {
			inputNode = new ConditionNodeModel(node.name, color);
		}

		return inputNode.getInPort ? <SRD.DefaultPortLabel model={inputNode.getInPort()} key="input" /> : null;
	}

	getYesPort() {
		const { node, color, displayOnly } = this.props;
		let outputNode = node;

		if (displayOnly) {
			outputNode = new ConditionNodeModel(node.name, color);
		}
		return outputNode.getYesPort ? <SRD.DefaultPortLabel model={outputNode.getYesPort()} key="yes-port" /> : null;
	}
	getNoPort() {
		const { node, color, displayOnly } = this.props;
		let outputNode = node;

		if (displayOnly) {
			outputNode = new ConditionNodeModel(node.name, color);
		}
		return outputNode.getNoPort ? <SRD.DefaultPortLabel model={outputNode.getNoPort()} key="no-port" /> : null;
	}
	onRemove() {
		const { node, diagramEngine, processStore } = this.props;
		node.remove();
		processStore.model.updateModel(diagramEngine.diagramModel.serializeDiagram());
		diagramEngine.repaintCanvas();
	}
	render() {
		const { node, displayOnly } = this.props;
		const { name } = node;

		return (
			<div className="basic-node__wrapper">
				<div className="basic-node basic-node--condition">
					<div className="input">
						<div className="dot-wrap">{this.getInPort()}</div>
					</div>
					{!displayOnly ? <div className="basic-node__delete fa fa-close" onClick={this.onRemove.bind(this)} /> : null}
					<div className="title--condition">
						<div className={!displayOnly ? 'name' : 'name ellipsis'} title={!displayOnly ? null : name}>
							{name}
						</div>
					</div>
					<div className="ports">
						<div className="yesport">{this.getYesPort()}</div>
						<div className="noport">{this.getNoPort()}</div>
					</div>
				</div>
			</div>
		);
	}
}

// export const ConditionNodeWidgetFactory = React.createFactory(ConditionNodeWidget);
