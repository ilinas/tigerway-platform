// libs
import React from 'react';
import * as SRD from 'storm-react-diagrams';

// src
import { ActionNodeWidget } from './ActionNodeWidget';
import { ActionNodeModel } from './ActionNodeModel';
const ActionNodeWidgetFactory = React.createFactory(ActionNodeWidget);

export class ActionWidgetFactory extends SRD.NodeFactory {
	constructor() {
		super('action');
	}
	generateReactWidget(diagramEngine, node) {
		return ActionNodeWidgetFactory({ node, diagramEngine });
	}
	getNewInstance() {
		return new ActionNodeModel();
	}
}
