// libs
import _ from 'lodash';
import * as SRD from 'storm-react-diagrams';

export class DiagramModel extends SRD.DiagramModel {
	constructor() {
		super();
		this.links = {};
		this.nodes = {};
		this.offsetX = 0;
		this.offsetY = 0;
		this.zoom = 100;
		this.rendered = false;
		this.name = '';
		this.gridSize = 0;
	}
	addLink(link) {
		link.addListener({
			entityRemoved: () => {
				this.removeLink(link);
			}
		});
		this.links[link.getID()] = link;
		this.iterateListeners(listener => {
			if (listener.linksUpdated) listener.linksUpdated(link, true);
		});
		return link;
	}
	setNewName(newName) {
		this.name = newName;
	}
	deSerializeDiagram(object, diagramEngine) {
		this.deSerialize(object);
		this.offsetX = object.offsetX;
		this.offsetY = object.offsetY;
		this.zoom = object.zoom;
		this.name = object.name;
		this.gridSize = object.gridSize;

		_.forEach(object.nodes, (node) => {
			let nodeOb = diagramEngine.getNodeFactory(node.type).getNewInstance(node);
			nodeOb.deSerialize(node);
			// deserialize ports
			_.forEach(node.ports, (port) => {
				let portOb = diagramEngine.getPortFactory(port.type).getNewInstance();
				portOb.deSerialize(port);
				nodeOb.addPort(portOb);
			});
			// console.log(nodeOb);
			this.addNode(nodeOb);
		});

		_.forEach(object.links, (link) => {
			let linkOb = diagramEngine.getLinkFactory(link.type).getNewInstance();
			linkOb.deSerialize(link);

			if (link.target) {
				linkOb.setTargetPort(this.getNode(link.target).getPortFromID(link.targetPort));
			}

			if (link.source) {
				// console.log('link', link);
				// console.log('this.getNode(link.source).getPortFromID(link.sourcePort)', this.getNode(link.source).getPortFromID(link.sourcePort));
				// console.log(' this.getNode', this.getNode(link.source));
				linkOb.setSourcePort(this.getNode(link.source).getPortFromID(link.sourcePort));
			}

			// console.log('linkOb', linkOb);
			this.addLink(linkOb);
		});
	}

	serializeDiagram() {
		return _.merge(this.serialize(), {
			offsetX: this.offsetX,
			offsetY: this.offsetY,
			zoom: this.zoom,
			name: this.name,
			gridSize: this.gridSize,
			links: _.map(this.links, link => {
				return link.serialize();
			}),
			nodes: _.map(this.nodes, link => {
				return link.serialize();
			})
		});
		// return {
		// 	...this.serialize(),
		// 	offsetX: this.offsetX,
		// 	offsetY: this.offsetY,
		// 	zoom: this.zoom,
		// 	name: this.name,
		// 	gridSize: this.gridSize,
		// 	links: _.map(this.links, link => link.serialize()),
		// 	nodes: _.map(this.nodes, link => link.serialize())
		// };
	}
}
