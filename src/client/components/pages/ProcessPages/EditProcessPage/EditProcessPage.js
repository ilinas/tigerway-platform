// libs
import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import { withRouter } from 'react-router';
import { Spin } from 'antd';
import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import cn from 'cn-decorator';
import { toJS } from 'mobx';
// src
import { Diagram } from './DiagramComponents/Diagram';
import './EditProcessPage.scss';
import { diagramEngine } from './DiagramComponents/Engine';
import { DiagramModel } from './DiagramComponents/DiagramModel';
import ControlsHeader from './ControlsHeader';
import TabsPanel from './TabsPanel';
import EditProcHelper from './EditProcHelper';
// let diagramModel = new DiagramModel();

@withRouter
@DragDropContext(HTML5Backend)
@inject('processStore')
@observer
@cn('edit-proc')
export default class EditProcessPage extends Component {
	async componentDidMount() {
		const { processStore, match } = this.props;
		const {  getProcess } = processStore;
		if (match.params.id === 'new') {
			// generateNewModel();
			this.setModel(processStore.model);
		}
		else {
			let proc = await getProcess(match.params.id);
			this.setModel(proc);
		}
		this.forceUpdate();
	}

	componentWillUnmount() {
		const { generateNewModel, model } = this.props.processStore;
		generateNewModel();
		this.setModel(model);
	}
	setModel = model => {
		const { updateModel } = this.props.processStore.model;

		let diagramModel = new DiagramModel();
		diagramModel.addListener({
			linksUpdated: event => {
				if (event.type === 'default') {
					event.addListener({
						targetPortChanged: port => {
							updateModel(diagramModel.serializeDiagram());
						}
					});
				}
				updateModel(diagramModel.serializeDiagram());
			},
			offsetUpdated: event => {
				updateModel(diagramModel.serializeDiagram());
			},
			nodesUpdated: event => {
				// if (event.entity.rendered) {
				// 	updateModel(event.entity.serializeDiagram());
				// }
			}
		});
		if (model) {
			diagramModel.deSerializeDiagram(toJS(model), diagramEngine);
		}
		this.addEventListnerOnNode(diagramModel);
		diagramEngine.setDiagramModel(diagramModel);
		diagramEngine.repaintCanvas();
	};
	addEventListnerOnNode(newDiagramModel) {
		const { model } = this.props.processStore;
		Object.values(newDiagramModel.getNodes()).map(node =>
			node.addListener({
				selectionChanged: (selectedNode, isSelected) => {
					if (selectedNode.isSelected) {
						console.log('de-dm', diagramEngine.diagramModel.serializeDiagram());
						// diagramEngine.diagramModel.clearSelection(selectedNode.entity);
						model.updateModel(diagramEngine.diagramModel.serializeDiagram());
					}
				}
			})
		);
	}
	render(cn) {
		const { processStore } = this.props;
		const { onNodeSelected } = processStore;
		const { updateModel } = processStore.model;
		// Render the canvas
		return (
			<section className={cn()}>
				<EditProcHelper />
				<Spin spinning={this.props.processStore.procsSpin}>
					<section className={cn('wrapper')}>
						<ControlsHeader
							forceUpdate={this.forceUpdate}
							diagramEngine={diagramEngine}
							setModel={this.setModel}/>
						<div className="parent-container">
							<Diagram
								diagramEngine={diagramEngine}
								diagramModel={diagramEngine.diagramModel}
								model={processStore.model}
								updateModel={updateModel}
								onNodeSelected={onNodeSelected}
								setModel={this.setModel}/>
							<TabsPanel setModel={this.setModel} updateModel={updateModel} diagramModel={diagramEngine.diagramModel} />
						</div>
					</section>
				</Spin>
			</section>
		);
	}
}
