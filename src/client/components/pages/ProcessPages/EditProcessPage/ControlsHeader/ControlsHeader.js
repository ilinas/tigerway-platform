// libs
import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import { withRouter } from 'react-router';
import { Button, Input } from 'antd';
import { applySnapshot } from 'mobx-state-tree';
// src
import './ControlsHeader.scss';
// import { recorder } from './../../../../../stores/mst/rootStore';

@withRouter
@inject('processStore')
@observer
export default class EditProcessPage extends Component {
	changeNameProcHandler = e => {
		const { processStore, diagramEngine } = this.props;
		diagramEngine.diagramModel.setNewName(e.target.value);
		processStore.model.updateModel(diagramEngine.diagramModel.serializeDiagram());
	};
	submit = async () => {
		const { history, processStore, diagramEngine } = this.props;
		processStore.model.updateModel(diagramEngine.diagramModel.serializeDiagram());
		if (history.location.pathname === '/proc/new') {
			let result = await processStore.saveProc();
			if (result) {
				history.push(`/proc/${this.props.processStore.model.id}`);
			}
		}
		else {
			processStore.updateProc();
		}
	};

	cancel = () => {
		const { history, processStore } = this.props;
		processStore.generateNewModel();
		history.push('/procs');
	};
	// prev() {
	// 	const { processStore, setModel } = this.props;
	// 	recorder.replaying = true;
	// 	applySnapshot(processStore.model, recorder.snapshots[--recorder.currentIndex]);
	// 	setModel(processStore.model.toJSON());
	// 	recorder.replaying = false;
	// }
	// next() {
	// 	const { processStore, setModel } = this.props;
	// 	recorder.replaying = true;
	// 	applySnapshot(processStore.model, recorder.snapshots[++recorder.currentIndex]);
	// 	setModel(processStore.model.toJSON());
	// 	recorder.replaying = false;
	// }
	render() {
		const { name } = this.props.processStore.model;
		return (
			<div className="controls--wrapper">
				<Button icon="left" onClick={this.cancel}>
					Назад
				</Button>
				<Button type="primary" onClick={this.submit}>
					Сохранить
				</Button>
				{/* <Button icon="caret-left" onClick={this.prev.bind(this)} disabled={recorder.currentIndex <= 0} />
				<Button
					icon="caret-right"
					onClick={this.next.bind(this)}
					disabled={recorder.currentIndex >= recorder.snapshots.length - 1}/> */}
				<Input value={name} onChange={this.changeNameProcHandler} placeholder="Название процесса" />
			</div>
		);
	}
}
