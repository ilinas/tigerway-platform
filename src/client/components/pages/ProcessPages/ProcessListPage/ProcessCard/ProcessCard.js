// lib
import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import { withRouter } from 'react-router';
import { Modal, Icon } from 'antd';
import cn from 'cn-decorator';
const confirm = Modal.confirm;
// src
import './ProcessCard.scss';

@withRouter
@inject('processStore')
@observer
@cn('process-block')
export default class ProcessCard extends Component {
	edit = e => {
		let target = e.target;
		let id = target.dataset.id;
		this.props.history.push(`/proc/${id}`);
	};
	showDeleteConfirm = e => {
		let target = e.target;
		let id = target.dataset.id;
		let store = this.props.processStore;
		confirm({
			title: 'Вы действительно хотите удалить этот процесс?',
			onOk() {
				store.deleteProcess(id);
			}
		});
	};
	render(cn) {
		const { id, name } = this.props;
		return (
			<div className={cn()}>
				<div className={cn('content')} onClick={this.edit} data-id={id}>
					<div className={cn('title')} data-id={id}>
						<span title={'Редактировать'} data-id={id}>
							{name}
						</span>
					</div>
				</div>
				<div className={cn('controls')}>
					<Icon className={cn('controls-item', { delete: true })} type="delete" title={'Удалить'} data-id={id} onClick={this.showDeleteConfirm.bind(this)} />
					{/* <Icon className={cn('controls-item', { settings: true })} type="setting" title={'Настройки'} data-id={id} onClick={this.settings} /> */}
					<Icon className={cn('controls-item', { edit: true })} type="edit" title={'Редактировать'} data-id={id} onClick={this.edit} />
				</div>
			</div>
		);
	}
}
