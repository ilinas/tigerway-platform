// lib
import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import { withRouter } from 'react-router';
import { Icon } from 'antd';
// import QueueAnim from 'rc-queue-anim';
import NProgress from 'nprogress';
import cn from 'cn-decorator';
// src
import ProcessCard from './ProcessCard';
import './ProcessListPage.scss';

@withRouter
@inject('processStore', 'paramsStore')
@observer
@cn('processes-page')
export default class ProcessListPage extends Component {
	async componentDidMount() {
		try {
			NProgress.start();
			await this.props.processStore.getAllProcesses();
		}
		finally {
			NProgress.done();
		}
	}
	submit = e => {
		e.preventDefault();
		e.stopPropagation();
		NProgress.done();
		this.props.history.push('/proc/new');
	};
	render(cn) {
		return (
			<div className={cn()}>
				<div className={cn('row')}>
					{/* <QueueAnim duration={2500} interval={300} className="processes-page__row"> */}
					<div className="add-button__block" onClick={this.submit}>
						<div className="add-button__content" title="Добавить новый процесс">
							<Icon className="add-button__icon" type="plus" />
						</div>
					</div>
					{this.props.processStore.processes.length > 0 &&
						this.props.processStore.processes.map(process => <ProcessCard key={process.id} {...process} />)}
					{/* </QueueAnim> */}
				</div>
			</div>
		);
	}
}
