import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import DevPage from './../../SystemPages/DevPage';
import './ConsolePage.scss';

@inject('userStore')
@observer
export default class ConsolePage extends Component {
	render() {
		return (
			<div className="admin-console__wrapper">
				<DevPage />
			</div>
		);
	}
}
