import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

@inject('userStore')
@observer
export default class isAdminShell extends Component {
	render() {
		// return this.props.userStore.userData.roles.indexOf('ROLE_ADMIN') !== -1 && this.props.children;
		return  this.props.children;
	}
}
