// lib
import React from 'react';
import { Button, Input, Form } from 'antd';
import { inject, observer } from 'mobx-react';
const FormItem = Form.Item;

@Form.create()
@inject('adminStore')
@observer
export default  class AddUserForm extends React.Component {
	handleSubmitForm = e => {
		const {form, adminStore} = this.props;
		e.preventDefault();
		form.validateFields((err, values) => {
			if (!err) {
				adminStore.addNewUser(values);
				form.resetFields();
			}
		});
	};
	render() {
		const { getFieldDecorator } = this.props.form;
		return (
			<Form hideRequiredMark={true} layout="inline" onSubmit={this.handleSubmitForm}>
				<FormItem label="Email">
					{getFieldDecorator('email', {
						rules: [{ required: true, message: 'Поле email является обязательным!' }, { type: 'email', message: 'Поле email является не валидным!' }]
					})(<Input placeholder="Email пользователя" />)}
				</FormItem>
				<FormItem label="Компания">
					{getFieldDecorator('companyName', {
						rules: [{ required: true, message: 'Поле компания является обязательным!' }]
					})(<Input placeholder="Название компании" />)}
				</FormItem>
				<FormItem>
					<Button htmlType="submit" type="primary">
						Добавить
					</Button>
				</FormItem>
			</Form>
		);
	}
}
