// lib
import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import cn from 'cn-decorator';
// src
import UsersTable from './UsersTable';
import AddUserForm from './AddUserForm';
import './UsersPage.scss';

@inject('userStore', 'adminStore')
@observer
@cn('users-page')
export default class AdminPage extends Component {
	componentWillMount() {
		this.props.adminStore.getListUsers();
	}
	render(cn) {
		return (
			<div className={cn()}>
				<div className={cn('user-form')}>
					<AddUserForm />
				</div>
				<div className={cn('user-table')}>
					<UsersTable />
				</div>
			</div>
		);
	}
}
