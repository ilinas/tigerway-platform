// lib
import React from 'react';
import { Input, Form } from 'antd';
import { inject, observer } from 'mobx-react';
const FormItem = Form.Item;

@Form.create()
@inject('adminStore')
@observer
export default class EditUserForm extends React.Component {
	render() {
		// const { getFieldDecorator } = this.props.form;
		const { editUser } = this.props.adminStore;
		// console.log(editUser);
		return (
			<Form hideRequiredMark={true}>
				<FormItem label="Email">
					{/* {getFieldDecorator('email', {
						initialValue: editUser.email,
						rules: [{ required: true, message: 'Поле email является обязательным!' }, { type: 'email', message: 'Поле email является не валидным!' }]
					})( */}
					<Input
						defaultValue={editUser.email}
						placeholder="Email пользователя"
						onChange={e => editUser.changeData('email', e.target.value)}/>
					{/* )} */}
				</FormItem>
				<FormItem label="Компания">
					{/* {getFieldDecorator('companyName', {
						initialValue: editUser.companyName,
						rules: [{ required: true, message: 'Поле компания является обязательным!' }]
					})( */}
					<Input
						defaultValue={editUser.companyName}
						placeholder="Название компании"
						onChange={e => editUser.changeData('companyName', e.target.value)}/>
					{/* )} */}
				</FormItem>
				<FormItem label="Пароль">
					{/* {getFieldDecorator('plainPassword', {
						initialValue: editUser.plainPassword,
						rules: [{ required: true, message: 'Поле компания является обязательным!' }]
					})( */}
					<Input
						defaultValue={editUser.plainPassword}
						placeholder="Пароль"
						onChange={e => editUser.changeData('plainPassword', e.target.value)}/>
					{/* )} */}
				</FormItem>
			</Form>
		);
	}
}
