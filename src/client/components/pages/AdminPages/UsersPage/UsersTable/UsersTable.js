// lib
import React from 'react';
import { Table, Input, Icon, Popconfirm, Modal } from 'antd';
import { TweenOneGroup } from 'rc-tween-one';
import { inject, observer } from 'mobx-react';
import cn from 'cn-decorator';
import EditUserForm from './EditUserForm';
// src
import './UsersTable.scss';

@inject('adminStore')
@observer
@cn('users-table')
export default class UsersTable extends React.Component {
	constructor(props) {
		super(props);
		this.enterAnim = [
			{ opacity: 0, x: 30, backgroundColor: '#fffeee', duration: 0 },
			{
				height: 0,
				duration: 200,
				type: 'from',
				delay: 250,
				ease: 'easeOutQuad',
				onComplete: this.onEnd
			},
			{ opacity: 1, x: 0, duration: 250, ease: 'easeOutQuad' },
			{ delay: 1000, backgroundColor: '#fff' }
		];
		this.leaveAnim = [{ duration: 250, opacity: 0 }, { height: 0, duration: 200, ease: 'easeOutQuad' }];

		this.currentPage = 1;
		this.newPage = 1;
	}
	state = {
		searchText: '',
		isShowEditModal: false
	};

	handleOk = e => {
		this.props.adminStore.updateUser();
		this.setState({
			isShowEditModal: false
		});
	};
	handleCancel = e => {
		this.setState({
			isShowEditModal: false
		});
	};
	getBodyWrapper = body => {
		return (
			<TweenOneGroup
				component="tbody"
				className={body.className}
				enter={this.enterAnim}
				leave={this.leaveAnim}
				appear={false}>
				{body.children}
			</TweenOneGroup>
		);
	};
	onSearch = e => {
		let searchField = e.target.dataset.field;
		this.props.adminStore.searchField = searchField;
		let searchText = this.state[`${searchField}`];
		this.props.adminStore.searchText = searchText;
	};
	onEnd = e => {
		const dom = e.target;
		dom.style.height = 'auto';
	};
	onInputChange = e => {
		let target = e.target;
		let searchField = target.dataset.field;
		let value = target.value;
		this.setState({ [`${searchField}`]: value });
	};
	onDelete = (id, e) => {
		e.preventDefault();
		this.props.adminStore.deleteUser(id);
	};
	pageChange = pagination => {
		this.newPage = pagination.current;
	};
	emitEmpty = e => {
		this.searchInput.focus();
		let searchField = e.target.dataset.field;
		this.setState({ [`${searchField}`]: '' });
	};
	edit(id) {
		this.props.adminStore.findEditUserById(id);
		this.setState({
			isShowEditModal: true
		});
	}
	render(cn) {
		const { isLoading, searchData } = this.props.adminStore;
		const columns = [
			{
				title: '#',
				dataIndex: 'number',
				key: 'number',
				sorter: (a, b) => a.number - b.number
			},
			{
				title: 'ID',
				dataIndex: 'id',
				key: 'id',
				sorter: (a, b) => a.id - b.id,
				filterIcon: <Icon type="search" style={{ color: this.state.filtered ? '#108ee9' : '#aaa' }} />,
				filterDropdownVisible: this.state.idFilterDropdownVisible,
				filterDropdown: (
					<div className="custom-filter-dropdown">
						<Input
							suffix={
								this.state.id ? (
									<div className="custom-filter-dropdown__controls">
										<Icon type="search" data-field="id" onClick={this.onSearch} />
										<Icon type="close-circle" data-field="id" onClick={this.emitEmpty} />
									</div>
								) : null
							}
							ref={ele => (this.searchInput = ele)}
							placeholder="Search id"
							value={this.state.id}
							onChange={this.onInputChange}
							onPressEnter={this.onSearch}
							data-field="id"/>
					</div>
				),
				onFilterDropdownVisibleChange: visible => {
					this.setState(
						{
							idFilterDropdownVisible: visible
						},
						() => this.searchInput.focus()
					);
				}
			},
			{
				title: 'Компания',
				dataIndex: 'companyName',
				key: 'companyName',
				filterIcon: <Icon type="search" style={{ color: this.state.filtered ? '#108ee9' : '#aaa' }} />,
				filterDropdownVisible: this.state.companyNameFilterDropdownVisible,
				filterDropdown: (
					<div className="custom-filter-dropdown">
						<Input
							suffix={
								this.state.companyName ? (
									<div className="custom-filter-dropdown__controls">
										<Icon type="search" data-field="companyName" onClick={this.onSearch} />
										<Icon type="close-circle" data-field="companyName" onClick={this.emitEmpty} />
									</div>
								) : null
							}
							ref={ele => (this.searchInput = ele)}
							placeholder="Search company"
							value={this.state.companyName}
							onChange={this.onInputChange}
							onPressEnter={this.onSearch}
							data-field="companyName"/>
					</div>
				),
				onFilterDropdownVisibleChange: visible => {
					this.setState(
						{
							companyNameFilterDropdownVisible: visible
						},
						() => this.searchInput.focus()
					);
				}
			},
			{
				title: 'Email',
				dataIndex: 'email',
				key: 'email',
				filterIcon: <Icon type="search" style={{ color: this.state.filtered ? '#108ee9' : '#aaa' }} />,
				filterDropdownVisible: this.state.emailFilterDropdownVisible,
				filterDropdown: (
					<div className="custom-filter-dropdown">
						<Input
							suffix={
								this.state.email ? (
									<div className="custom-filter-dropdown__controls">
										<Icon type="search" data-field="email" onClick={this.onSearch} />
										<Icon type="close-circle" data-field="email" onClick={this.emitEmpty} />
									</div>
								) : null
							}
							ref={ele => (this.searchInput = ele)}
							placeholder="Search email"
							value={this.state.email}
							onChange={this.onInputChange}
							onPressEnter={this.onSearch}
							data-field="email"/>
					</div>
				),
				onFilterDropdownVisibleChange: visible => {
					this.setState(
						{
							emailFilterDropdownVisible: visible
						},
						() => this.searchInput.focus()
					);
				}
			},
			{
				title: 'Action',
				dataIndex: '',
				key: 'x',
				width: 200,
				render: (text, record, index) => {
					return (
						<div className="editable-row-operations">
							<a onClick={() => this.edit(record.id)}>Редактировать</a>
							<span className="ant-divider" />
							<Popconfirm
								title="Вы действительно хотите удалить?"
								onConfirm={e => this.onDelete(record.id, e)}
								cancelText="Отмена"
								okText="Удалить">
								<span className={cn('button', { delete: true })}>Удалить</span>
							</Popconfirm>
						</div>
					);
				}
			}
		];
		return (
			<div>
				<Table
					components={{
						body: {
							wrapper: this.getBodyWrapper,
							cell: this.getBodyCellWrapper
						}
					}}
					locale={{
						emptyText: (
							<span>
								<Icon type="frown-o" /> Нет данных
							</span>
						)
					}}
					loading={isLoading}
					columns={columns}
					pagination={{ pageSize: 15 }}
					dataSource={searchData.toJSON()}
					className={cn()}
					onChange={this.pageChange}/>
				<Modal
					destroyOnClose={true}
					title="Помощь"
					visible={this.state.isShowEditModal}
					onOk={this.handleOk}
					onCancel={this.handleCancel}>
					<EditUserForm />
				</Modal>
			</div>
		);
	}
}
