// libs
import React, { Component } from 'react';
import cn from 'cn-decorator';
import { Row, Col } from 'antd';
// src
import AnswerCard from './AnswerCard';
import './FaqPage.scss';

@cn('faq-page')
export default class FaqPage extends Component {
	render(cn) {
		const data = [
			{
				id: 1,
				question: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit?',
				answer: 'BEligendi amet quam inventore nam nostrum quaerat aliquam enim dicta illo laboriosam, quia odio voluptas animi consequatur non nemo deleniti, illum tempore assumenda reprehenderit corporis vel impedit nihil earum. Voluptas explicabo voluptatem iste libero officia cum voluptate, qui laboriosam atque.'
			},
			{
				id: 2,
				question: 'Doloribus dolores, officia voluptatibus deserunt ratione debitis laboriosam?',
				answer:
					'Ut tempora, ad eos. Eveniet asperiores quaerat cupiditate quo, possimus officiis deserunt porro dolor mollitia iure minima id incidunt facilis accusamus ea quod perferendis veniam quas, et distinctio dolores corporis magni sapiente hic. Non saepe, veritatis, molestias debitis illum, quasi optio modi numquam harum repellat dolorem, velit blanditiis aliquid eveniet iusto! Sed reiciendis, tempora ex dolor fugiat temporibus, iste!'
			},
			{
				id: 3,
				question: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit?',
				answer: 'BEligendi amet quam inventore nam nostrum quaerat aliquam enim dicta illo laboriosam, quia odio voluptas animi consequatur non nemo deleniti, illum tempore assumenda reprehenderit corporis vel impedit nihil earum. Voluptas explicabo voluptatem iste libero officia cum voluptate, qui laboriosam atque.'
			},
			{
				id: 4,
				question: 'Doloribus dolores, officia voluptatibus deserunt ratione debitis laboriosam?',
				answer:
					'Ut tempora, ad eos. Eveniet asperiores quaerat cupiditate quo, possimus officiis deserunt porro dolor mollitia iure minima id incidunt facilis accusamus ea quod perferendis veniam quas, et distinctio dolores corporis magni sapiente hic. Non saepe, veritatis, molestias debitis illum, quasi optio modi numquam harum repellat dolorem, velit blanditiis aliquid eveniet iusto! Sed reiciendis, tempora ex dolor fugiat temporibus, iste!'
			}
		];
		const data2 = [
			{
				id: 1,
				question: 'Doloribus dolores, officia voluptatibus deserunt ratione debitis laboriosam?',
				answer:
					'Ut tempora, ad eos. Eveniet asperiores quaerat cupiditate quo, possimus officiis deserunt porro dolor mollitia iure minima id incidunt facilis accusamus ea quod perferendis veniam quas, et distinctio dolores corporis magni sapiente hic. Non saepe, veritatis, molestias debitis illum, quasi optio modi numquam harum repellat dolorem, velit blanditiis aliquid eveniet iusto! Sed reiciendis, tempora ex dolor fugiat temporibus, iste! Ut tempora, ad eos. Eveniet asperiores quaerat cupiditate quo, possimus officiis deserunt porro dolor mollitia iure minima id incidunt facilis accusamus ea quod perferendis veniam quas, et distinctio dolores corporis magni sapiente hic. Non saepe, veritatis, molestias debitis illum, quasi optio modi numquam harum repellat dolorem, velit blanditiis aliquid eveniet iusto! Sed reiciendis, tempora ex dolor fugiat temporibus, iste! '
			},
			{
				id: 2,
				question: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit?',
				answer: 'BEligendi amet quam inventore nam nostrum quaerat aliquam enim dicta illo laboriosam, quia odio voluptas animi consequatur non nemo deleniti, illum tempore assumenda reprehenderit corporis vel impedit nihil earum. Voluptas explicabo voluptatem iste libero officia cum voluptate, qui laboriosam atque.'
			},
			{
				id: 3,
				question: 'Doloribus dolores, officia voluptatibus deserunt ratione debitis laboriosam?',
				answer:
					'Ut tempora, ad eos. Eveniet asperiores quaerat cupiditate quo, possimus officiis deserunt porro dolor mollitia iure minima id incidunt facilis accusamus ea quod perferendis veniam quas, et distinctio dolores corporis magni sapiente hic. Non saepe, veritatis, molestias debitis illum, quasi optio modi numquam harum repellat dolorem, velit blanditiis aliquid eveniet iusto! Sed reiciendis, tempora ex dolor fugiat temporibus, iste! Ut tempora, ad eos. Eveniet asperiores quaerat cupiditate quo, possimus officiis deserunt porro dolor mollitia iure minima id incidunt facilis accusamus ea quod perferendis veniam quas, et distinctio dolores corporis magni sapiente hic. Non saepe, veritatis, molestias debitis illum, quasi optio modi numquam harum repellat dolorem, velit blanditiis aliquid eveniet iusto! Sed reiciendis, tempora ex dolor fugiat temporibus, iste!'
			},
			{
				id: 4,
				question: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit?',
				answer: 'BEligendi amet quam inventore nam nostrum quaerat aliquam enim dicta illo laboriosam, quia odio voluptas animi consequatur non nemo deleniti, illum tempore assumenda reprehenderit corporis vel impedit nihil earum. Voluptas explicabo voluptatem iste libero officia cum voluptate, qui laboriosam atque.'
			}
		];
		return (
			<div className={cn()}>
				<div className={cn('header')}>
					<h1 className={cn('title')}>FAQ</h1>
				</div>
				<div className={cn('body')}>
					<Row gutter={32}>
						<Col xs={24} sm={24} md={12} lg={12}>
							{data.map((item, index) => (
								<div key={item.id}>
									<AnswerCard question={item.question} answer={item.answer} />
									{index !== data.length - 1 && <div className="divider divider-xl divider-dashed" />}
								</div>
							))}
						</Col>
						<Col xs={24} sm={24} md={12} lg={12}>
							{data2.map((item, index) => (
								<div key={item.id}>
									<AnswerCard question={item.question} answer={item.answer} />
									{index !== data.length - 1 && <div className="divider divider-xl divider-dashed" />}
								</div>
							))}
						</Col>
					</Row>
				</div>
			</div>
		);
	}
}
