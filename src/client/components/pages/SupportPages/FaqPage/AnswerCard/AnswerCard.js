// libs
import React, { Component } from 'react';
import cn from 'cn-decorator';
// src
import './AnswerCard.scss';

@cn('answer-card')
export default class AnswerCard extends Component {
	render(cn) {
		const { question, answer } = this.props;
		return (
			<div className={cn()}>
				<h4 className={cn('question')}>{question}</h4>
				<p className={cn('answer')}>{answer}</p>
			</div>
		);
	}
}
