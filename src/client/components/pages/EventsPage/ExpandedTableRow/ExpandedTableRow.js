import React, { Component } from 'react';
import { Row, Col, Timeline } from 'antd';

import Card from './../../../common/Card';
import './../EventsPage.scss';

export default class ExpandedTableRow extends Component {
	render() {
		const { proc } = this.props;
		return (
			<Row gutter={16}>
				<Col span={12}>
					<Card title={'История выполнения нод'}>
						<Timeline>
							{proc.nodeEvents.map(nodeEvent => (
								<Timeline.Item color={nodeEvent.success === true ? 'green' : 'red'} key={nodeEvent.id}>
									{nodeEvent.resultMessage} ({nodeEvent.dateCreate})
								</Timeline.Item>
							))}
						</Timeline>
					</Card>
				</Col>
				<Col span={12}>
					<Card title={'Полученные данные'}>
						{proc.data.apikey && <p>
							<span className="text--main">Ключ:</span> {proc.data.apikey}
						</p>}
						{proc.data.apikey && <hr />}
						<p className="text--main">Параметры:</p>
						{Object.keys(proc.data.params).map(param => (
							<p key={param}>
								{param}: {proc.data.params[param]}
							</p>
						))}
						<hr />
						<p className="text--main">Информация:</p>
						{Object.keys(proc.data.info).map(infoParam => (
							<p key={infoParam}>
								{infoParam}: {proc.data.info[infoParam]}
							</p>
						))}
					</Card>
				</Col>
			</Row>
		);
	}
}
