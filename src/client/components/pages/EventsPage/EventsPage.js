import React, { Component } from 'react';
import './EventsPage.scss';
import { Table, Icon, Button } from 'antd';
import { inject, observer } from 'mobx-react';
import ExpandedTableRow from './ExpandedTableRow';
import NProgress from 'nprogress';

@inject('eventsStore')
@observer
export default class EventsPage extends Component {
	componentDidMount() {
		try {
			this.props.eventsStore.getEventsData();
		}
		finally {
			NProgress.done();
		}
	}
	render() {
		const { procEvents, isLoading } = this.props.eventsStore;
		const columns = [
			{ title: 'Id', dataIndex: 'id', key: 'id' },
			{ title: 'Название', dataIndex: 'proc.name', key: 'name' },
			{ title: 'Имя контакта', dataIndex: 'contact.name', key: 'contact.name' },
			{ title: 'Телефон конакта', dataIndex: 'contact.phone', key: 'contact.phone' },
			{ title: 'Email контакта', dataIndex: 'contact.email', key: 'contact.email' },
			{ title: 'Дата', dataIndex: 'dateCreate', key: 'dateCreate' },
			{
				title: 'Actions',
				key: 'Actions',
				render: (text, record, index) => {
					return <Button onClick={() => console.log('procEvent', record)}>Лог</Button>;
				}
			}
		];
		return (
			<div className="events-page">
				<Table
					locale={{
						emptyText: (
							<span>
								<Icon type="frown-o" /> Нет данных
							</span>
						)
					}}
					loading={isLoading}
					columns={columns}
					dataSource={procEvents.toJSON()}
					expandedRowRender={record => <ExpandedTableRow proc={record} />}/>
			</div>
		);
	}
}
