// libs
import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router';
import { Form, Icon, Input, Button } from 'antd';
const FormItem = Form.Item;
import cn from 'cn-decorator';
// src
import './ForgotPasswordPage.scss';

@withRouter
@inject('userStore', 'notifyStore')
@observer
@cn('forgotpass-page')
class ForgotPasswordPage extends Component {
	state = {
		loading: false
	};
	handleSubmit = e => {
		e.preventDefault();
		this.props.form.validateFields((err, values) => {
			if (!err) {
				console.log('Received values of form: ', values);
			}
		});
	};
	enterLoading = () => {
		this.setState({ loading: true });
	};
	render(cn) {
		const { getFieldDecorator } = this.props.form;
		return (
			<div className={cn()}>
				<Form onSubmit={this.handleSubmit} className={cn('reset-form')}>
					<img className={cn('main-image')} role="presentation" src="./images/logo_medium.png" />
					<FormItem hasFeedback>
						{getFieldDecorator('login', {
							rules: [{ required: true, message: 'Please input your username!' }]
						})(<Input prefix={<Icon type="user" style={{ fontSize: 13 }} />} placeholder="Логин" />)}
						<div className={cn('help-msg')}>Введите ваш email, на него будет выслано письмо с дальнейшими инструкциями.</div>
					</FormItem>
					<FormItem>
						<Button loading={this.state.loading} onClick={this.enterLoading} type="primary" htmlType="submit" className={cn('btn', { reset: true })}>
							Восстановить
						</Button>
					</FormItem>
				</Form>
			</div>
		);
	}
}
const WrappedForgotPasswordPage = Form.create()(ForgotPasswordPage);
export default WrappedForgotPasswordPage;
