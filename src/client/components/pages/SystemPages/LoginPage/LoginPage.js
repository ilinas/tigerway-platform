// libs
import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router';
import { Form, Icon, Input, Button, Checkbox } from 'antd';
const FormItem = Form.Item;
import cn from 'cn-decorator';
// src
import './LoginPage.scss';
import logoMediumSrc from './../../../images/logo_medium.png';

@Form.create()
@withRouter
@inject('userStore', 'notifyStore')
@observer
@cn('login-page')
export default class LoginPage extends Component {
	state = {
		loading: false,
		iconLoading: false
	};

	handleSubmit = e => {
		e.preventDefault();
		this.props.form.validateFields((err, values) => {
			if (!err) {
				console.log('Received values of form: ', values);
				this.props.userStore
					.loginUser(values)
					.then(() => {
						this.setState({ loading: false });
						this.props.history.push('/procs');
					})
					.catch(() => {
						this.setState({ loading: false });
					});
			}
		});
	};
	enterLoading = () => {
		this.setState({ loading: true });
	};
	goToResetPas = e => {
		e.preventDefault();
		this.props.history.push('/reset');
	};
	enterIconLoading = () => {
		this.setState({ iconLoading: true });
	};
	render(cn) {
		const { getFieldDecorator } = this.props.form;
		return (
			<div className={cn()}>
				<Form onSubmit={this.handleSubmit} className={cn('login-form')}>
					<img className={cn('main-image')} role="presentation" src={logoMediumSrc} />
					<FormItem hasFeedback>
						{getFieldDecorator('login', {
							rules: [{ required: true, message: 'Поле логин обязательно!' }]
						})(<Input prefix={<Icon type="user" style={{ fontSize: 13 }} />} placeholder="Логин" />)}
					</FormItem>
					<FormItem hasFeedback>
						{getFieldDecorator('password', {
							rules: [{ required: true, message: 'Поле пароль обязательно!' }]
						})(<Input prefix={<Icon type="lock" style={{ fontSize: 13 }} />} type="password" placeholder="Пароль" />)}
					</FormItem>
					<FormItem>
						{getFieldDecorator('remember', {
							valuePropName: 'checked',
							initialValue: true
						})(<Checkbox>Запомнить меня</Checkbox>)}
						<a className={cn('btn', { forgot: true })} onClick={this.goToResetPas}>
							Забыли пароль?
						</a>
						<Button loading={this.state.loading} onClick={this.enterLoading} type="primary" htmlType="submit" className={cn('btn', { login: true })}>
							Войти
						</Button>
					</FormItem>
				</Form>
			</div>
		);
	}
}
