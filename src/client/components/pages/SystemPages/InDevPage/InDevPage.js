import React, { Component } from 'react';
import './InDevPage.scss';
import DevPageImageSrc1 from './devPage1.png';
import DevPageImageSrc2 from './devPage1.png';
import DevPageImageSrc3 from './devPage1.png';

export default class InDevPage extends Component {
	getRandomInt(min, max) {
		let newMin = Math.ceil(min);
		let newMax = Math.floor(max);
		return Math.floor(Math.random() * (newMax - newMin)) + newMin;
	}
	getPageImage() {
		let num = this.getRandomInt(1, 4);
		return `DevPageImageSrc${num}`;
	}
	render() {
		return (
			<div className="dev-wrapper">
				<section className="dev-container">
					<h1 className="dev-container__title">В разработке</h1>
					<img src={this.getPageImage()} />
				</section>
			</div>
		);
	}
}
