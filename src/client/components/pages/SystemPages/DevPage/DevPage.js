import React, { Component } from 'react';
import './DevPage.scss';

export default class DevPage extends Component {
	getImagePath(index) {
		let requireContext = require.context('./../../../images', true, /devPage.\.png/);
		return requireContext.keys().map(requireContext)[index];
	}
	getRandomInt(min, max) {
		let newMin = Math.ceil(min);
		let newMax = Math.floor(max);
		return Math.floor(Math.random() * (newMax - newMin)) + newMin;
	}
	render() {
		return (
			<div className="dev-wrapper">
				<section className="dev-container">
					<h1 className="dev-container__title">В разработке</h1>
					<img src={this.getImagePath(this.getRandomInt(0, 3))} />
				</section>
			</div>
		);
	}
}
