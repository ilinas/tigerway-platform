import React, { Component } from 'react';

export default class NotFoundPage extends Component {
	render() {
		return (
			<div className="not-found-page">
				<h1 className="not-found-page__title">404</h1>
			</div>
		);
	}
}
