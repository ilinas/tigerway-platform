// libs
import React, { Component } from 'react';
import {inject} from 'mobx-react';
@inject('userStore')

export default class BaseShell extends Component {
	componentWillMount() {
		this.props.userStore.startStore();
	}
	render() {
		return this.props.children;
	}
}
