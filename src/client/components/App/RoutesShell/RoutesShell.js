// lib
import React, { Component } from 'react';
import { inject } from 'mobx-react';
import { withRouter } from 'react-router';
import { NavLink } from 'react-router-dom';
import { Layout, Menu, Icon, BackTop } from 'antd';
const { SubMenu, Item } = Menu;
const { Sider } = Layout;
import cn from 'cn-decorator';
import NProgress from 'nprogress';
// src
import ErrorWrapper from './../../common/ErrorBoudary';
import Header from './../../common/Header';
import './RoutesShell.scss';

@inject('userStore', 'paramsStore', 'processStore', 'testStore')
@withRouter
@cn('routes-wrapper')
export default class RoutesShell extends Component {
	state = {
		userData: false,
		selectedKeys: []
	};
	componentDidMount() {
		try {
			NProgress.start();
			let userData = this.props.userStore.getUserData();
			this.setState({ userData, selectedKeys: [this.props.history.location.pathname] });
			this.props.processStore.getAllData();
			this.props.paramsStore.getAllData();
		}
		finally {
			NProgress.done();
		}
	}
	componentWillReceiveProps() {
		this.setState({ selectedKeys: [this.props.history.location.pathname] });
	}
	render(cn) {
		return (
			<div className={cn()}>
				<Header />
				<Layout className={cn('main-layout')}>
					<Sider width={220} collapsible style={{ background: '#fff' }} breakpoint="lg" collapsedWidth="0">
						<Menu
							mode="inline"
							defaultSelectedKeys={['/procs']}
							className={cn('menu')}
							inlineIndent={30}
							defaultOpenKeys={['sub1', 'sub3', 'sub4']}
							selectedKeys={this.state.selectedKeys}>
							{/* <Item key="/home" >
								<Icon type="home" />
								<span className={cn('menu-item')}>Рабочий стол</span>
							</Item> */}
							<Item key="/procs">
								<NavLink to={'/procs'}>
									<Icon type="appstore" />
									<span className={cn('menu-item')}>Процессы</span>
								</NavLink>
							</Item>
							<Item key="/events">
								<NavLink to={'/events'}>
									<Icon type="book" />
									<span className={cn('menu-item')}>История</span>
								</NavLink>
							</Item>
							{/* <Item key="/widgets">
								<Icon type="tool" />
								<span className={cn('menu-item')}>Виджеты</span>
							</Item> */}
							<SubMenu
								key="sub3"
								title={
									<h4>
										<Icon type="notification" />
										<span className={cn('menu-item')}>Интеграции</span>
									</h4>
								}>
								<Item key="/integration/amo">
									<NavLink to={'/integration/amo'}>
										<span className={cn('menu-item')}>Amo CRM</span>
									</NavLink>
								</Item>
								<Item key="/integration/site">
									<NavLink to={'/integration/site'}>
										<span className={cn('menu-item')}>Сайт</span>
									</NavLink>
								</Item>
								<Item key="/integration/email">
									<NavLink to={'/integration/email'}>
										<span className={cn('menu-item')}>Почта</span>
									</NavLink>
								</Item>
								<Item key="/integration/sms">
									<NavLink to={'/integration/sms'}>
										<span className={cn('menu-item')}>SMS</span>
									</NavLink>
								</Item>
								{/* <Item key="/integration/googleDrive">
									<span className={cn('menu-item')}>GoogleDrive</span>
								</Item> */}
							</SubMenu>
							{this.state.userData &&
								this.state.userData.roles.indexOf('ROLE_ADMIN') !== -1 && (
									<SubMenu
										key="sub1"
										title={
											<h4>
												<Icon type="safety" />
												<span className={cn('menu-item')}>Админка</span>
											</h4>
										}>
										<Item key="/admin/users">
											<NavLink to={'/admin/users'}>
												<span className={cn('menu-item')}>Пользователи</span>
											</NavLink>
										</Item>
										<Item key="/admin/console">
											<span className={cn('menu-item')}>Консоль</span>
										</Item>
									</SubMenu>
								)}
							{/* <SubMenu
								key="sub4"
								title={
									<h4>
										<Icon type="setting" />
										<span className={cn('menu-item')}>Настройки</span>
									</h4>
								}>
								<Item key="/settings/params">
									<span className={cn('menu-item')}>Параметры</span>
								</Item>
							</SubMenu>
							<Divider />
							<Item key="/support/faq">
								<Icon type="question" />
								<span className={cn('menu-item')}>FAQ</span>
							</Item>
							<Item key="/support/contactwithus" >
								<Icon type="customer-service" />
								<span className={cn('menu-item')}>Связаться с нами</span>
							</Item> */}
						</Menu>
					</Sider>
					<ErrorWrapper>
						<div className={cn('main-content')}>
							{this.props.children}
						</div>
					</ErrorWrapper>
					<BackTop />
				</Layout>
			</div>
		);
	}
}
