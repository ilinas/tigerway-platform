// libs
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'mobx-react';

// components
import BaseShell from './BaseShell';
import './App.scss';

// stores
import rootStore from './../../stores/mst/rootStore';

import userStore from './../../stores/mobx/userStore';

const stores = {
	userStore
};

export default class App extends Component {
	static propTypes = {
		children: PropTypes.object.isRequired
	};

	render() {
		return (
			<Provider {...stores} {...rootStore}>
				<BaseShell>{this.props.children}</BaseShell>
			</Provider>
		);
	}
}
