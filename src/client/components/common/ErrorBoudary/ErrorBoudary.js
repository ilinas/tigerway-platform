import React, { Component } from 'react';
import { withRouter } from 'react-router';
import Result from 'ant-design-pro/lib/Result';
import { Button, Icon } from 'antd';

import './ErrorBoundary.scss';

@withRouter
export default class ErrorWrapper extends Component {
	state = {
		hasError: false,
		error: null,
		info: null
	};
	componentDidMount() {
		this.unlisten = this.props.history.listen(() => {
			this.setState({
				hasError: false
			});
		});
	}
	componentWillUnmount() {
		this.unlisten();
	}
	componentDidCatch(error, info) {
		this.setState({
			hasError: true,
			error: JSON.stringify(error),
			info: JSON.stringify(info)
		});
	}

	render() {
		if (this.state.hasError) {
			const { error, info } = this.state;
			const extra = (
				<div>
					<div style={{ fontSize: 16, color: 'rgba(0, 0, 0, 0.85)', fontWeight: 500, marginBottom: 16 }}>
						Выполните действия：
					</div>
					<div style={{ marginBottom: 16 }}>
						<Icon style={{ color: '#f5222d', marginRight: 8 }} type="close-circle-o" />Перезагрузите страницу
					</div>
					<div>
						<Icon style={{ color: '#f5222d', marginRight: 8 }} type="close-circle-o" />Свяжитесь с администратором info@tigerway.ru
					</div>
				</div>
			);

			return (
				// <div className={'error-boundary'}>
				// 	<div>Something went wrong here...</div>
				// 	<div>Error: "{error}"</div>
				// 	<div>Additional info: "{info}"</div>
				// </div>
				<div className={'error-boundary'}>
					<Result
						type="error"
						title="Что-то пошло не так..."
						extra={extra}/>
				</div>

			);
		}

		return this.props.children;
	}
}
