import React, { Component } from 'react';
import { Icon } from 'antd';
import './PageHelper.scss';

export default class PageHelper extends Component {
	render() {
		const {onClick, className} = this.props;
		return (
			<span className={`page-helper ${className}`} onClick={onClick} >
				<Icon type="question-circle-o" />
			</span>
		);
	}
}
