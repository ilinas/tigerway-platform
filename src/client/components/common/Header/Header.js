import React, { Component } from 'react';
import { Layout, Avatar, Dropdown, Menu } from 'antd';
const { Header } = Layout;
import { withRouter } from 'react-router';
import { inject, observer } from 'mobx-react';
import { NavLink } from 'react-router-dom';
import { Row, Col } from 'antd';
import './Header.scss';
import logoMediumSrc from './../../images/logo_medium.png';

@withRouter
@inject('userStore')
@observer
export default class CustomHeader extends Component {
	state = {
		userData: {
			login: false
		}
	};
	componentDidMount() {
		let userData = this.props.userStore.getUserData();
		this.setState({ userData });
	}
	logout = () => {
		this.props.history.push('/login');
		this.props.userStore.resetUserData();
	};
	render() {
		return (
			<Header className="header">
				<Row align="middle" justify="center">
					<Col span={14}>
						<NavLink to="/" className="logo__wrapper">
							<img className="logo" role="presentation" src={logoMediumSrc} />
						</NavLink>
					</Col>
					<Col span={10}>
						<div className="user-logo">
							{this.props.userStore.isSessionCorrect && (
								<Dropdown
									overlay={
										<Menu>
											<Menu.Item>
												<a rel="noopener noreferrer">Профиль</a>
											</Menu.Item>
											<Menu.Item>
												<a rel="noopener noreferrer" onClick={this.logout}>
															Выход
												</a>
											</Menu.Item>
										</Menu>
									}
									placement="bottomLeft">
									<Avatar size="large">
										<span className="user-name">{this.state.userData.login && this.state.userData.login[0]}</span>
									</Avatar>
								</Dropdown>
							)}
						</div>
					</Col>
				</Row>
			</Header>
		);
	}
}
