import React, { Component } from 'react';
import { INPUTTYPES } from './../../../constants/inputTypesConstant';
import moment from 'moment';
import { Input, Select, InputNumber, DatePicker, Radio, Switch, Tooltip, Icon } from 'antd';
const Option = Select.Option;
const { TextArea } = Input;
const RadioGroup = Radio.Group;
import { observer  } from 'mobx-react';

@observer
export default class ParamTypeSwitch extends Component {
	render() {
		const { param, paramType } = this.props;
		if (param) {
			switch (paramType.paramValueTypeCode) {
			case INPUTTYPES.text.code:
				return (
					<Input
						value={param.values.toJSON()[0] || ''}
						onChange={e => {
							param.setNewValue([e.target.value]);
						}}
						placeholder={`Введите ${paramType.name.toLowerCase()}`}/>
				);
			case INPUTTYPES.numeric.code:
				return (
					<InputNumber
						value={param.values.toJSON()[0] || ''}
						onChange={e => param.setNewValue([e])}
						min={0}/>
				);
			case INPUTTYPES.checkbox.code:
				return <Switch checked={param.values.toJSON()[0]} onChange={e => param.setNewValue([e])} />;
			case INPUTTYPES.select.code:
				return (
					<Select
						allowClear
						value={param.values.toJSON()[0]}
						onChange={e => param.setNewValue([e])}
						placeholder={`Введите ${paramType.name.toLowerCase()}`}>
						{paramType.paramOptions.map(option => (
							<Option key={option.id} value={option.id}>
								{option.value}
							</Option>
						))}
					</Select>
				);
			case INPUTTYPES.multiselect.code:
				return (
					<Select
						value={param.values.toJSON()}
						onChange={e => param.setNewValue(e.map(item => item * 1))}
						placeholder={`Введите ${paramType.name.toLowerCase()}`}
						mode="multiple">
						{paramType.paramOptions.map(option => (
							<Option key={option.id} value={option.id}>
								{option.value}
							</Option>
						))}
					</Select>
				);
			case INPUTTYPES.date.code:
				return (
					<DatePicker
						format={'DD.MM.YYYY'}
						value={param.values.toJSON()[0] && moment(param.values.toJSON()[0], 'DD.MM.YYYY')}
						// value={param.values.toJSON()[0] && moment(param.values.toJSON()[0], 'DD.MM.YYYY') : moment()}
						onChange={(date, dateString) => param.setNewValue([dateString])}/>
				);
			case INPUTTYPES.textarea.code:
				return (
					<TextArea
						value={param.values.toJSON()[0] || ''}
						onChange={e => param.setNewValue([e.target.value])}
						placeholder={`Введите ${paramType.name.toLowerCase()}`}
						rows={3}/>
				);
			case INPUTTYPES.radio.code:
				return (
					<div>
						<RadioGroup value={`${param.values.toJSON()[0]}`} onChange={e => param.setNewValue([e.target.value * 1])}>
							{paramType.paramOptions.map(option => (
								<Radio key={option.id} value={`${option.id}`}>
									{option.value}
								</Radio>
							))}
						</RadioGroup>
						<Tooltip placement="topRight" title={'Очистить выбранное значение'} arrowPointAtCenter>
							<Icon className="pointer" type="close-circle-o" onClick={() => param.setNewValue([])}/>
						</Tooltip>
					</div>
				);
			default:
				return (
					<Input
						value={param.values.toJSON()[0] || ''}
						onChange={e => param.setNewValue([e.target.value])}
						placeholder={`Введите ${paramType.name.toLowerCase()}`}/>
				);
			}
		}
	}
}
