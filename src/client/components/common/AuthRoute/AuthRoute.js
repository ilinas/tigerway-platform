import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import PropTypes from 'prop-types';
import { inject, observer } from 'mobx-react';
import { Button } from 'antd';
import Exception from 'ant-design-pro/lib/Exception';
import { withRouter } from 'react-router';

@withRouter
@inject('userStore')
@observer
export default class AuthRoute extends Component {
	static propTypes = {
		userStore: PropTypes.object.isRequired
	};
	render() {
		const { userStore, component, children, history } = this.props;
		const { isSessionCorrect, isInitialized } = userStore;

		if (!isInitialized) return null;

		return isSessionCorrect ? (
			<Route {...this.props} component={component}>
				{children}
			</Route>
		) : (
			<Exception
				type="403"
				title={'401'}
				desc={'Доступ закрыт. Повторно выполните вход в систему.'}
				actions={
					<Button size="large" type="primary" onClick={() => history.push('/login')}>
						Перейти на страницу входа
					</Button>
				}/>
		);
	}
}
