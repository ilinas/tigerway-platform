import React, { Component } from 'react';
import * as CodeMirror from 'codemirror';
import 'codemirror/lib/codemirror.css';
import 'codemirror/theme/monokai.css';
import './TemplateEditor.scss';

export default class TemplateEditor extends Component {
	componentDidMount() {
		this.editor = CodeMirror.fromTextArea(this.refs.editor);
		this.editor.on('change', this.handleChange);
	}

	handleChange = e => {
		let inputValue = this.editor.getValue();
		let reg = /{{([^}{]+)}}/g;
		let correctMatches = inputValue.match(reg);
		if (correctMatches !== null) {
			correctMatches.forEach(match => {
				let firstIndex = inputValue.indexOf(match);
				let lastIndex = firstIndex + match.length;
				let widget = document.createElement('span');
				let titleWidget = match.replace(/\{\{|\}\}/g, '');
				widget.innerHTML = `<div class='cm-widget__body'><span class='cm-widget__node-name'>${titleWidget}</span></div>`;
				e.doc.markText({ line: 0, ch: firstIndex }, { line: 0, ch: lastIndex }, { replacedWith: widget });
			});
		}
	};

	render() {
		return (
			<div className="template-editor">
				<textarea ref="editor" className="" defaultValue={this.props.defaultValue} />
			</div>
		);
	}
}
