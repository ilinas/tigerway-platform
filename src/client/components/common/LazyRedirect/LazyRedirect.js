import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { inject, observer } from 'mobx-react';

@inject('userStore')
@observer
export default class LazyRedirect extends Component {
	render() {
		const { userStore } = this.props;
		const { isInitialized } = userStore;

		return !isInitialized ? null : <Redirect to={'/procs'} />;
	}
}
