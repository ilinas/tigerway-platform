// libs
import React, { Component } from 'react';
import cn from 'cn-decorator';
// src
import './Accordion.scss';

@cn('accordion')
export default class Accordion extends Component {
	state = {
		show: false
	};
	toggle = () => this.setState({ show: !this.state.show });
	render(cn) {
		return (
			<div className={cn()}>
				<div onClick={this.toggle} className={cn('header')}>
					{this.state.show ? <i className="fa fa-chevron-down" /> : <i className="fa fa-chevron-right" />}
					<span className={cn('title')}>{this.props.title}</span>
				</div>
				<div className={this.state.show ? cn('content') : 'hide'}>
					<div style={this.props.isChild ? this.props.style : null}>{this.props.children}</div>
				</div>
			</div>
		);
	}
}
