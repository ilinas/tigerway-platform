// libs
import React from 'react';
import { Input, Select } from 'antd';
import { observer, inject } from 'mobx-react';
const Option = Select.Option;
// src
import ParamTypeSwitch from './../../ParamTypeSwitch';
import './ValidationFormItem.scss';

@inject('processStore', 'paramsStore', 'validationStore')
@observer
export default class ValidationFormItem extends React.Component {
	constructor(props) {
		super(props);
		const value = this.props.value || {};
		this.state = {
			validationTypeCode: value.validationTypeCode || '',
			params: value.params || ''
		};
	}
	componentWillReceiveProps(nextProps) {
		// Should be a controlled component.
		if ('value' in nextProps) {
			const value = nextProps.value;
			this.setState(value);
		}
	}
	handleValidationTypeChange = e => {
		let validationTypeCode = e;
		if (!('value' in this.props)) {
			this.setState({ validationTypeCode });
		}
		this.triggerChange({ validationTypeCode });
	};
	handleOptionChange = (e, validationOptionCode) => {
		if (e.target.value) {
			this.triggerChange({ params: { [validationOptionCode]: e.target.value } });
		}
		else {
			this.triggerChange({ params: { [validationOptionCode]: e } });
		}
	};
	getParams(validationTypeCode) {
		if (validationTypeCode !== '') {
			let validationType = this.props.validationStore.validationTypes.find(item => item.code === validationTypeCode);
			if (validationType) {
				let paramTypeCodes = validationType.paramTypeCodes;
				let validationOptions = [];
				paramTypeCodes.map(ptc => {
					this.props.paramsStore.paramTypes.map(pt => {
						if (ptc === pt.code) {
							validationOptions.push(pt);
						}
					});
				});
				return validationOptions;
			}
		}
		return [];
	}
	triggerChange = changedValue => {
		const onChange = this.props.onChange;
		if (onChange) {
			onChange(Object.assign({}, this.state, changedValue));
		}
	};
	render() {
		const state = this.state;
		const { validationTypesCodes } = this.props;
		return (
			<div className="validation-item__wrapper">
				<Select value={state.validationTypeCode} onChange={this.handleValidationTypeChange}>
					{validationTypesCodes.length > 0 &&
						validationTypesCodes.map(validationType => (
							<Option key={validationType.id} value={validationType.code}>
								{validationType.name}
							</Option>
						))}
				</Select>
				{this.getParams(state.validationTypeCode).length > 0 && this.getParams(state.validationTypeCode).map(validationOption => <ParamTypeSwitch mode={'params'} value={state.params[validationOption.code]} onChange={e => this.handleOptionChange(e, validationOption.code)} key={validationOption.code} paramType={validationOption} />)}
			</div>
		);
	}
}
