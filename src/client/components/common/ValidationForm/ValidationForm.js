// libs
import React from 'react';
import { Form, Icon, Button, Checkbox, Input } from 'antd';
import { observer, inject } from 'mobx-react';
const FormItem = Form.Item;
// src
import ValidationFormItem from './ValidationFormItem';
import './ValidationForm.scss';

@inject('processStore', 'validationStore', 'paramsStore')
@observer
export default class ValidationForm extends React.Component {
	state = {
		formulaValue: '',
		showFormula: false
	};
	getFormulaValue() {
		const { form } = this.props;
		const keys = form.getFieldValue('keys');
		this.setState({ formulaValue: keys.join(' AND ') });
	}
	remove = (fromRemove, removeItemkey) => {
		const { form } = this.props;
		const formItems = form.getFieldValue(fromRemove);
		form.setFieldsValue({
			[fromRemove]: formItems.filter(key => key !== removeItemkey)
		});
	};
	onChangeCheckboxHandler = () => {
		this.setState({ showFormula: !this.state.showFormula });
	};
	add = fromAdd => {
		const { form, mode } = this.props;
		const formItems = form.getFieldValue(fromAdd);
		const nextFormItems = formItems.concat(formItems.length + 1);
		form.setFieldsValue({
			[fromAdd]: nextFormItems
		});
	};

	render() {
		const { mode, validationTypesCodes, form, initialValidationState, paramsStore } = this.props;
		const { getFieldDecorator, getFieldValue } = this.props.form;
		const { validationTypes } = this.props.validationStore;
		let { showFormula, formulaValue } = this.state;
		const formItemLayout = {
			labelCol: {
				xs: { span: 24 },
				sm: { span: 1 }
			},
			wrapperCol: {
				xs: { span: 24 },
				sm: { span: 23 }
			}
		};
		const formItemLayoutWithOutLabel = {
			wrapperCol: {
				xs: { span: 24, offset: 0 }
				// sm: { span: 22, offset: 2 }
			}
		};
		let groups = [];
		let formItems = [];
		if (mode === 'groups') {
			getFieldDecorator('groups', { initialValue: [] });
			groups = getFieldValue('groups');
		}
		else {
			let initialValue = [];
			if (initialValidationState) {
				initialValue = initialValidationState.map((item, index) => index + 1);
			}
			getFieldDecorator('formItems', { initialValue });
			formItems = getFieldValue('formItems');
		}
		return (
			<div className="validation-form">
				{mode === 'groups' ? (
					groups.map((group, i) => {
						getFieldDecorator(`group-${group}`, { initialValue: [1] });
						const keys = getFieldValue(`group-${group}`);
						return (
							<FormItem label={`Группа №${i + 1}`} {...formItemLayoutWithOutLabel} required={false} key={group}>
								{getFieldDecorator(`group-${group}`)(
									<div>
										{keys.map((k, index) => (
											<FormItem {...formItemLayout} label={index + 1} required={false} key={k}>
												{getFieldDecorator(`param${group}-${k}`)(<ValidationFormItem validationTypesCodes={validationTypesCodes} style={{ width: '60%', marginRight: 8 }} />)}
												{keys.length > 1 ? <Icon className="dynamic-delete-button" type="minus-circle-o" disabled={keys.length === 1} onClick={() => this.remove(`group-${group}`, k)} /> : null}
											</FormItem>
										))}
										<Button type="dashed" onClick={() => this.add(`group-${group}`)} style={{ width: '100%' }}>
											<Icon type="plus" /> Добавить правило в группу
										</Button>
									</div>
								)}
								{groups.length > 1 ? <Icon className="dynamic-delete-button" type="minus-circle-o" disabled={groups.length === 1} onClick={() => this.remove('groups', group)} /> : null}
							</FormItem>
						);
					})
				) : (
					formItems.map((formItem, i) => {
						let validationTypeCode = initialValidationState[i] ? initialValidationState[i].validationTypeCode : '';
						return (
							<FormItem {...formItemLayout} label={i + 1} key={formItem}>
								{getFieldDecorator(`param-${formItem}`, {
									initialValue: {
										validationTypeCode
										// params: paramsStore.mapValidationParams(this.props.validationStore.validationTypesCodes[i], initialValidationState[i].params)
									}
								})(<ValidationFormItem validationTypesCodes={validationTypesCodes} form={form} />)}
								{formItems.length > 1 ? <Icon className="dynamic-delete-button" type="minus-circle-o" disabled={formItem.length === 1} onClick={() => this.remove('formItems', formItem)} /> : null}
							</FormItem>
						);
					})
				)}
				{/*
					formula section
					(type === 'condition' || type === 'trigger') &&
				keys.length > 1 && (
					<FormItem {...formItemLayoutWithOutLabel}>
						<Checkbox value={showFormula} onClick={this.onChangeCheckboxHandler} style={{ width: '100%' }}>
							Показать формулу
						</Checkbox>
						{getFieldDecorator('formula', {
							initialValue: formulaValue
						})(<Input className={showFormula ? '' : 'hide'} />)}
					</FormItem>
				)
				*/}
				<FormItem {...formItemLayoutWithOutLabel}>
					<Button type="dashed" onClick={() => this.add(mode !== 'groups' ? 'formItems' : 'groups')} style={{ width: '100%' }}>
						<Icon type="plus" /> {mode !== 'groups' ? 'Добавить правило' : 'Добавить группу правил'}
					</Button>
				</FormItem>
			</div>
		);
	}
}
