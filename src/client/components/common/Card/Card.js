// libs
import React, { Component } from 'react';
// src
import './Card.scss';

export default class Card extends Component {
	getCardBody(mode, children) {
		switch (mode) {
		case 'list':
			return children;
		case 'params':
			return <div className="card__body">{children}</div>;
		case 'editor':
			return <div className="card__body--editor">{children}</div>;
		case 'search':
			return children;
		default:
			return <div className="card__body">{children}</div>;
		}
	}
	render() {
		const { title, children, mode } = this.props;
		return (
			<div className={'card'}>
				{title && <div className="card__head">
					<h3>{title}</h3>
				</div>}
				{this.getCardBody(mode, children)}
			</div>
		);
	}
}
