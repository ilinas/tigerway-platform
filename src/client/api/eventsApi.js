import baseApi from './baseApi';

export const eventsApi = {
	getEventsData() {
		return baseApi
			.ajax({
				method: 'get',
				url: '/proc-events'
			})
			.then(res => res.data);
	}
};
