import axios from 'axios';
// import { config } from './apiConfig';
import rootStore from './../stores/mst/rootStore';
import userStore from './../stores/mobx/userStore';

const config = window.appConfig;

const baseApi = {
	ajax(request) {
		const promise = axios({ baseURL: config.baseURL, ...request });

		promise
			.then(res => {
				if (res.data.message) {
					rootStore.notifyStore.showSucces('Успех', res.data.message);
				}
				return res;
			})
			.catch(error => {
				if (error.response && error.response.data.status === 401) {
					userStore.isSessionCorrect = false;
					rootStore.notifyStore.showError('Ошибка', error.response.data.message);
				}
				else if (error.response && error.response.data.message) {
					rootStore.notifyStore.showError('Ошибка', error.response.data.message);
				}
				else {
					rootStore.notifyStore.showError();
				}
				return error;
			});


		return promise;
	},
	setAuthorizationHeader(token) {
		axios.defaults.headers.common = { Authorization: `Bearer ${token}` };
	}
};

export default baseApi;
