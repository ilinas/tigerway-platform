import baseApi from './baseApi';

export const masterdataApi = {
	getAllData() {
		return baseApi
			.ajax({
				method: 'get',
				url: '/masterdata/data'
			})
			.then(res => res.data);
	}
};
