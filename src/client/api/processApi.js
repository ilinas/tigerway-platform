import baseApi from './baseApi';

export const processApi = {
	getAllData() {
		return baseApi
			.ajax({
				method: 'get',
				url: '/graph/data'
			})
			.then(res => res.data);
	},
	getAllProcesses() {
		return baseApi
			.ajax({
				method: 'get',
				url: '/procs'
			})
			.then(res => res.data);
	},
	getProcess(id) {
		return baseApi
			.ajax({
				method: 'get',
				url: `/procs/${id}`
			})
			.then(res => res.data);
	},
	createProcess(data) {
		return baseApi
			.ajax({
				method: 'post',
				url: '/procs',
				data
			})
			.then(res => res.data);
	},
	updateProcess(data) {
		return baseApi
			.ajax({
				method: 'put',
				url: '/procs',
				data
			})
			.then(res => res.data);
	},
	deleteProcess(id) {
		return baseApi
			.ajax({
				method: 'delete',
				url: `/procs/${id}`
			})
			.then(res => res.data);
	}
};
