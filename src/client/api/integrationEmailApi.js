import baseApi from './baseApi';

export const integrationEmailApi = {
	getIntegrationEmailData() {
		return baseApi
			.ajax({
				method: 'get',
				url: '/integration/email'
			})
			.then(res => res.data);
	},
	setIntegrationEmailData(data) {
		return baseApi
			.ajax({
				method: 'post',
				data,
				url: '/integration/email'
			})
			.then(res => res.data);
	},
	clearEmail() {
		return baseApi
			.ajax({
				method: 'delete',
				url: '/integration/email'
			})
			.then(res => res.data);
	}
};
