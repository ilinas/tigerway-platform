import baseApi from './baseApi';

export const integrationAmoApi = {
	getIntegrationAmoData() {
		return baseApi
			.ajax({
				method: 'get',
				url: '/integration/amo/accounts'
			})
			.then(res => res.data);
	},
	saveIntegrationAmoData(amoData) {
		return baseApi
			.ajax({
				method: 'post',
				url: '/integration/amo/accounts',
				data: amoData
			})
			.then(res => res.data);
	},
	updateIntegrationAmoData(amoData) {
		return baseApi
			.ajax({
				method: 'put',
				url: `/integration/amo/accounts/${amoData.id}`,
				data: amoData
			})
			.then(res => res.data);
	},
	refreshAmoData(amoDataId) {
		return baseApi
			.ajax({
				method: 'post',
				url: `/integration/amo/accounts/update-schema/${amoDataId}`
			})
			.then(res => res.data);
	},
	clearAmoData(amoDataId) {
		return baseApi
			.ajax({
				method: 'delete',
				url: `/integration/amo/accounts/${amoDataId}`
			})
			.then(res => res.data);
	}
};
