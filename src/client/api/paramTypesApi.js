import baseApi from './baseApi';

export const paramTypesApi = {
	getAllParams() {
		return baseApi
			.ajax({
				method: 'get',
				url: '/settings/param-types'
			})
			.then(res => res.data);
	},
	createParam(data) {
		return baseApi
			.ajax({
				method: 'post',
				url: '/settings/param-types',
				data
			})
			.then(res => res.data);
	},
	updateParam(data) {
		return baseApi
			.ajax({
				method: 'put',
				url: `/settings/param-types/${data.id}`,
				data
			})
			.then(res => res.data);
	},
	deleteParam(id) {
		return baseApi
			.ajax({
				method: 'delete',
				url: `/settings/param-types/${id}`
			})
			.then(res => res.data);
	}
};
