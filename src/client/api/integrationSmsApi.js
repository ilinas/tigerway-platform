import baseApi from './baseApi';

export const integrationSmsApi = {
	getIntegrationSmsData() {
		return baseApi
			.ajax({
				method: 'get',
				url: '/integration/sms'
			})
			.then(res => res.data);
	},
	setIntegrationSmsData(data) {
		return baseApi
			.ajax({
				method: 'post',
				data,
				url: '/integration/sms'
			})
			.then(res => res.data);
	},
	clearSmsData() {
		return baseApi
			.ajax({
				method: 'delete',
				url: '/integration/sms'
			})
			.then(res => res.data);
	}
};
