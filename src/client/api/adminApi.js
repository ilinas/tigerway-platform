import baseApi from './baseApi';

export const adminApi = {
	getAllAccounts() {
		return baseApi
			.ajax({
				method: 'get',
				url: '/admin/accounts'
			})
			.then(res =>
				res.data.data.accounts.map((item, index) => {
					return {
						number: index + 1,
						id: item.id,
						companyName: item.companyName,
						plainPassword: item.plainPassword,
						email: item.email,
						roles: item.roles
					};
				})
			);
	},

	createAccount(account) {
		return baseApi
			.ajax({
				method: 'post',
				url: '/admin/accounts',
				data: account
			})
			.then(res => res.data);
	},

	upadateAccount(account) {
		return baseApi
			.ajax({
				method: 'put',
				url: `/admin/accounts/${account.id}`,
				data: {
					companyName: account.companyName,
					// password: account.password || '',
					plainPassword: account.plainPassword || '',
					email: account.email
				}
			})
			.then(res => res.data);
	},
	deleteAccount(id) {
		return baseApi
			.ajax({
				method: 'delete',
				url: `/admin/accounts/${id}`
			})
			.then(res => res.data);
	}
};
