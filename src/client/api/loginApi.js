import baseApi from './baseApi';
import userStore from './../stores/mobx/userStore';

export const loginApi = {
	doLogIn(login, password) {
		return baseApi
			.ajax({
				method: 'post',
				url: '/tokens',
				data: {
					email: login,
					plainPassword: password
				}
			})
			.then(res => res.data)
			.then(getProcessResponseFn(login));
	},

	doLogOut() {
		return new Promise(res => {
			res();
		}).then(() => {
			userStore.resetUserData();
		});
	}
};

function getProcessResponseFn(login) {
	userStore.isSessionCorrect = true;
	return data => {
		const { token } = data.data;
		const { roles } = data.data.account;
		const userData = {
			login,
			token,
			roles
		};
		userStore.setUserData(userData);
	};
}
