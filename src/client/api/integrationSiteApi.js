import baseApi from './baseApi';

export const integrationSiteApi = {
	getIntegrationSiteData() {
		return baseApi
			.ajax({
				method: 'get',
				url: '/integration/site'
			})
			.then(res => res.data);
	}
};
