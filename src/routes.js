// libs
import React from 'react';
import { Redirect } from 'react-router';
import Loadable from 'react-loadable';
import { report } from 'import-inspector';
import path from 'path';
// src
import AuthRoute from './client/components/common/AuthRoute';
import LazyRedirect from './client/components/common/LazyRedirect';
import HomePage from './client/components/pages/HomePage';
import RoutesShell from './client/components/App/RoutesShell';
import isAdminShell from './client/components/pages/AdminPages/isAdminShell';
import IntegrationPagesShell from './client/components/pages/IntegrationPages/IntegrationPagesShell';
import SupportShell from './client/components/pages/SupportPages/SupportShell';
import SettingsShell from './client/components/pages/SettingsPages/SettingsShell';
import NotFoundPage from './client/components/pages/SystemPages/NotFoundPage';

if (typeof System === 'undefined' || typeof System.import !== 'function') {
	global.System = {
		import: importPath => {
			return Promise.resolve(require(importPath));
		}
	};
}

const routes = [
	{
		path: '/login',
		component: Loadable({
			loader: () =>
				System.import(/* webpackChunkName: 'LoginPage' */ './client/components/pages/SystemPages/LoginPage'),
			modules: ['./client/components/pages/SystemPages/LoginPage'],
			webpack: () => [require.resolveWeak('./client/components/pages/SystemPages/LoginPage')],
			loading: () => <div>Loading...</div>
		})
	},
	{
		path: '/reset',
		component: Loadable({
			loader: () =>
				System.import(
					/* webpackChunkName: 'ForgotPasswordPage' */ './client/components/pages/SystemPages/ForgotPasswordPage'
				),
			modules: ['./client/components/pages/SystemPages/ForgotPasswordPage'],
			webpack: () => [require.resolveWeak('./client/components/pages/SystemPages/ForgotPasswordPage')],
			loading: () => <div>Loading...</div>
		})
	},
	{
		path: '/',
		component: RoutesShell,
		routes: [
			{
				path: '/',
				exact: true,
				component: () => <LazyRedirect />
			},
			{
				path: '/home',
				exact: true,
				component: () => <AuthRoute to={'/home'} component={HomePage} />
			},
			{
				path: '/procs',
				exact: true,
				component: () => (
					<AuthRoute
						to={'/procs'}
						component={Loadable({
							loader: () =>
								System.import(
									/* webpackChunkName: 'ProcessPage' */ './client/components/pages/ProcessPages/ProcessListPage'
								),
							loading: () => <div>Loading...</div>
						})}/>
				)
			},
			{
				path: '/dev',
				exact: true,
				component: () => (
					<AuthRoute
						to={'/dev'}
						component={Loadable({
							loader: () =>
								System.import(
									/* webpackChunkName: 'ProcessPage' */ './client/components/pages/DevPage'
								),
							loading: () => <div>Loading...</div>
						})}/>
				)
			},
			{
				path: '/events',
				exact: true,
				component: () => (
					<AuthRoute
						to={'/events'}
						component={Loadable({
							loader: () =>
								System.import(
									/* webpackChunkName: 'EventsPage' */ './client/components/pages/EventsPage'
								),
							loading: () => <div>Loading...</div>
						})}/>
				)
			},
			{
				path: '/proc/:id',
				component: () => (
					<AuthRoute
						to={'/proc/:id'}
						component={Loadable({
							loader: () =>
								System.import(
									/* webpackChunkName: 'EditProcessPage' */ './client/components/pages/ProcessPages/EditProcessPage'
								),
							loading: () => <div>Loading...</div>
						})}/>
				)
			},
			{
				path: '/widgets',
				exact: true,
				component: () => (
					<AuthRoute
						to={'/widgets'}
						component={Loadable({
							loader: () =>
								System.import(
									/* webpackChunkName: 'WidgetsPage' */ './client/components/pages/WidgetsPage'
								),
							loading: () => <div>Loading...</div>
						})}/>
				)
			},
			{
				path: '/integration',
				component: IntegrationPagesShell,
				routes: [
					{
						path: '/integration/amo',
						exact: true,
						component: () => (
							<AuthRoute
								to={'/integration/amo'}
								component={Loadable({
									loader: () =>
										System.import(
											/* webpackChunkName: 'AmoIntegration' */ './client/components/pages/IntegrationPages/AmoIntegration'
										),
									loading: () => <div>Loading...</div>
								})}/>
						)
					},
					{
						path: '/integration/site',
						exact: true,
						component: () => (
							<AuthRoute
								to={'/integration/site'}
								component={Loadable({
									loader: () =>
										System.import(
											/* webpackChunkName: 'SiteIntegration' */ './client/components/pages/IntegrationPages/SiteIntegration'
										),
									loading: () => <div>Loading...</div>
								})}/>
						)
					},
					{
						path: '/integration/sms',
						exact: true,
						component: () => (
							<AuthRoute
								to={'/integration/email'}
								component={Loadable({
									loader: () =>
										System.import(
											/* webpackChunkName: 'SmsIntegration' */ './client/components/pages/IntegrationPages/SmsIntegration'

										),
									loading: () => <div>Loading...</div>
								})}/>
						)
					},
					{
						path: '/integration/email',
						exact: true,
						component: () => (
							<AuthRoute
								to={'/integration/email'}
								component={Loadable({
									loader: () =>
										System.import(
											/* webpackChunkName: 'EmailIntegration' */ './client/components/pages/IntegrationPages/EmailIntegration'
										),
									loading: () => <div>Loading...</div>
								})}/>
						)
					},
					{
						path: '/integration/googleDrive',
						exact: true,
						component: Loadable({
							loader: () =>
								System.import(
									/* webpackChunkName: 'GoogleDriveIntegration' */ './client/components/pages/IntegrationPages/GoogleDriveIntegration'
								),
							loading: () => <div>Loading...</div>
						})
					},
					{
						path: '/integration/',
						exact: true,
						component: () => <Redirect to="/integration/amo" />
					}
				]
			},
			{
				path: '/admin',
				component: isAdminShell,
				routes: [
					{
						path: '/admin/users',
						exact: true,
						component: () => (
							<AuthRoute
								to={'/admin/users'}
								component={Loadable({
									loader: () =>
										System.import(
											/* webpackChunkName: 'AdminUsersPage' */ './client/components/pages/AdminPages/UsersPage'
										),
									loading: () => <div>Loading...</div>
								})}/>
						)
					},
					{
						path: '/admin/console',
						exact: true,
						component: () => (
							<AuthRoute
								to={'/admin/console'}
								component={Loadable({
									loader: () =>
										System.import(
											/* webpackChunkName: 'AdminConsolePage' */ './client/components/pages/AdminPages/ConsolePage'
										),
									loading: () => <div>Loading...</div>
								})}/>
						)
					},
					{
						path: '/admin/',
						exact: true,
						component: () => <Redirect to="/admin/users" />
					}
				]
			},
			{
				path: '/support',
				component: SupportShell,
				routes: [
					{
						path: '/support/faq',
						exact: true,
						component: () => (
							<AuthRoute
								to={'/support/faq'}
								component={Loadable({
									loader: () =>
										System.import(
											/* webpackChunkName: 'SupportFaqPage' */ './client/components/pages/SupportPages/FaqPage'
										),
									loading: () => <div>Loading...</div>
								})}/>
						)
					},
					{
						path: '/support/',
						exact: true,
						component: () => <Redirect to="/support/faq" />
					}
				]
			},
			{
				path: '/settings',
				component: SettingsShell,
				routes: [
					{
						path: '/settings/params',
						exact: true,
						component: () => (
							<AuthRoute
								to={'/settings/params'}
								component={Loadable({
									loader: () =>
										System.import(
											/* webpackChunkName: 'SettingsParamsPage' */ './client/components/pages/SettingsPages/ParamsPage'
										),
									loading: () => <div>Loading...</div>
								})}/>
						)
					},
					{
						path: '/settings/',
						exact: true,
						component: () => <Redirect to="/settings/params" />
					}
				]
			}
		]
	},
	{
		path: '/404',
		exact: true,
		component: NotFoundPage
	},
	{
		path: '/*',
		redirectTo: '/404', // explicit flag for ssr processing on server.js
		component: () => <Redirect to="/404" />
	}
];

export default routes;
